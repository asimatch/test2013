<%@page import="todo.dao.TodoDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="todo.vo.TodoValueObject"%>
<%//@page import="todo.vo.TodoValueObject"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>

<SCRIPT src="detail.js"></SCRIPT>
<SCRIPT src="batch.js"></SCRIPT>
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script>
$(function(){
	$("#room").click(function(){
		window.open("floor.jsp");
	});
	$("#states").click(function(){
		window.open("states.jsp");
	});
	$("#link").click(function(){
		window.open("link.jsp");
	});
	$("#phone").click(function(){
		window.open("contact.jsp");
	});

});
function jump(id){
 	location.href = "empStates.jsp?Emp=" + id;
}
</script>


<style>
<!--
.body{
  background-color:blue;
}

.scr {
  overflow: scroll;   /* スクロール表示 */
  border: 2px;
  width: 200px%;
  height: 100px;
}

.ontheBox{
text-align: left;
}

.textboxTable{
border: 2px;
width: 100%;
align: right;

}
-->
</style>

</head>
<body>
<h1>Be-Board</h1>

<form action="search" method="get" style="float: left;">
<select name="pull">
<%
TodoValueObject room = (TodoValueObject)request.getAttribute("roomInfo");
request.getSession().setAttribute("roomID", room.getRoomID());
int width = room.getWidth();
int height = room.getHeight();

List<TodoValueObject> roomAll = (List<TodoValueObject>)request.getAttribute("room");
request.getSession().setAttribute("room", roomAll);
for(TodoValueObject roomvo : roomAll){
%>
<option value=<%=roomvo.getRoomID()%>><%=roomvo.getRoomName() %></option>
<%} %>
</select>
<input type="submit" value="移動">
</form>
<%=room.getDepartmentName() %>

<div class="aa"></div>

	<%
		ArrayList<Integer> readFLG = (ArrayList<Integer>)request.getAttribute("readFLG");
		
		List<TodoValueObject> empList = (List<TodoValueObject>)request.getAttribute("empList");
			    
		// listを二次元配列に置き換
		String b[][] = new String[empList.size()][10];
		TodoValueObject empInfo[] = new TodoValueObject[empList.size()];
		int index1 = 0;
		int index2 = 0;
		for(TodoValueObject vo : empList){
			b[index1][index2++] = String.valueOf(vo.getEmployeeID());
			b[index1][index2++] = vo.getFirstName();
			b[index1][index2++] = String.valueOf(vo.getTableX());
			b[index1][index2++] = String.valueOf(vo.getTableY());
			b[index1][index2++] = String.valueOf(vo.getHeight());
			b[index1][index2++] = String.valueOf(vo.getWidth());
			b[index1][index2++] = vo.getColor();
			b[index1][index2++] = vo.getIpAddress();
			b[index1][index2++] = vo.getLocalPhoneNumber();
			//未読メッセージがあるか
			if(readFLG.indexOf(vo.getEmployeeID()) != -1){
				b[index1][index2] = "true";
			}else{
				b[index1][index2] = "false";
			}
			empInfo[index1] = vo;
			index1++;
			index2 = 0;
		}

		List<TodoValueObject> statusList = (List<TodoValueObject>)request.getAttribute("status");
		String statusMaster[][] = new String[statusList.size()][3];
		int index7 = 0;
		int index8 = 0;
		for(TodoValueObject vo : statusList){
			statusMaster[index7][index8++] = vo.getStatus();
			statusMaster[index7][index8++] = vo.getColor();
			statusMaster[index7][index8] = String.valueOf(vo.getStatusID());
			index7++;
			index8 = 0;
		}
		
		List<TodoValueObject> bulletin = (List<TodoValueObject>)request.getAttribute("bulletin");
		String messages[][] = new String[bulletin.size()][3];
		int index3 = 0;
		int index4 = 0;
		for(TodoValueObject vo : bulletin){
			messages[index3][index4++] = String.valueOf(vo.getUpdate());
			messages[index3][index4++] = vo.getFirstName();
			messages[index3][index4] = String.valueOf(vo.getBulletin());
			index3++;
			index4 = 0;
		}

		List<TodoValueObject> rentalPhone = (List<TodoValueObject>)request.getAttribute("rentalPhone");
		String telMap[][] = new String[rentalPhone.size()][3];
		int index5 = 0;
		int index6 = 0;
		for(TodoValueObject vo : rentalPhone){
			telMap[index5][index6++] = vo.getTelNo();
			telMap[index5][index6++] = String.valueOf(vo.getEmployeeID());
			telMap[index5][index6] = vo.getMemo();
			index5++;
			index6 = 0;
		}

		int listCounter = 0;//ループの中での無駄を省く変数
		int[][][] throughList = new int[height][width][1];
		for(int i=0; i<throughList.length; i++){
			for(int j = 0; j < throughList[i].length; j++){
			throughList[i][j][0] = 0;
			}
		}//throughリスト(1*1以上の席の大きさを持つ社員がいる場合のレイアウト調整用配列)の作成および初期化
%>



<table  border=2px class=textboxTable>
<caption>社内掲示板</caption>
<tr>
<th>
<div class="scr">
<p class="ontheBox">
<%for(int i = 0; i < messages.length; i++){ %>
<%=messages[i][0]+"　" %>
<%=messages[i][1]+"　" %>
<%=messages[i][2]+"　" %>
<br>
<%}%>
</div>
</th>
</tr>
</table>

<table border = 2;>
<%for(int i = 0; i < height; i++){%>
<tr height="40">
<%for(int j = 0; j < width; j++){
%>
<%if(listCounter < b.length ){ %>

<%if(Integer.parseInt(b[listCounter][2])==i && Integer.parseInt(b[listCounter][3])==j && throughList[i][j][0] == 0){%>
<th bgColor=<%if(b[listCounter][6] != null ){ %><%= b[listCounter][6] %><%}else{%>#FFFFFF<%}%> width="100"rowspan=<%=Integer.parseInt(b[listCounter][4])%> colspan=<%=Integer.parseInt(b[listCounter][5])%> onmouseover="getDetail('<%=b[listCounter][0]%>')" onclick="jump('<%=b[listCounter][0]%>')">
<%if(b[listCounter][9].equals("true"))out.println(" !");%><a href="messageRead?num=<%=i%>_<%=j%>"><%=b[listCounter][1]%></a><%-- <a href="detail?num=<%=i%>_<%=j%>"><font size =2px>　編集</font></a> --%>
<%=b[listCounter][8] %>

<%request.getSession().setAttribute("EmployeeID" + i + "_" + j, b[listCounter][0]);%>
</th>
<%
if(Integer.parseInt(b[listCounter][4]) > 1 && Integer.parseInt(b[listCounter][5]) == 1){
	int deleteY = Integer.parseInt(b[listCounter][4]) - 1;
	while(deleteY > 0 ){
	throughList[i+deleteY][j][0] = 1;
	deleteY--;
	}
}%>



<% if(Integer.parseInt(b[listCounter][5]) > 1 && Integer.parseInt(b[listCounter][4]) ==1){
	int deleteX = Integer.parseInt(b[listCounter][5]) - 1;
	while(deleteX > 0){
	throughList[i][j+deleteX][0] = 1;
	deleteX--;
	}
}%>

<%if(Integer.parseInt(b[listCounter][4]) > 1 && Integer.parseInt(b[listCounter][5]) >1){
	int deleteX = Integer.parseInt(b[listCounter][5]) - 1;
	while(deleteX > 0){
	throughList[i][j+deleteX][0] = 1;
	deleteX--;
	}
	int deleteY = Integer.parseInt(b[listCounter][4]) - 1;
	while(deleteY > 0 ){
	deleteX = Integer.parseInt(b[listCounter][5]) - 1;
	throughList[i+deleteY][j][0] = 1;
	while(deleteX > 0){
		throughList[i+deleteY][j+deleteX][0] = 1;
		deleteX--;
	}
	deleteY--;
	}
}
%>

<%listCounter++;
  }else if(throughList[i][j][0] == 0){%>
<th width="100"><%=i %>_<%=j %></th>
<%}%>
<%}else if(throughList[i][j][0] == 0){%>

<%}%>
<%}%>
</tr>
<%}%>

</table>

<table  border=1px >
<caption>状態の凡例</caption>
<tr>
<%for(int i = 0; i < statusMaster.length; i++){ %>
<th bgColor=#<%=statusMaster[i][1] %>>
<%=statusMaster[i][0] %>
</th>
<%} %>
</tr>
</table>
<table>
<tr>
<th>
<form action="registerEmpStates" method="get">
<select name="states">
<%for(int i = 0; i < statusMaster.length; i++){ %>
<option value=<%=statusMaster[i][2]%>><%=statusMaster[i][0] %></option>
<%} %>
</select>
<input type="hidden" id="ids" name="empIDs">
<input type="submit" value="更新">
<div id=names></div>
</form>
</th>
</tr>
</table>
<div id ="result"></div>
<br>
<Table width = 100% border = 2px>
<tr>
<th>
<input type="submit" name="update" value="画面更新" id="update">
</th>
<th>
<input type="submit" name="room" value="部屋編集" ID="room">
</th>
<th>
<input type="submit" name="link" value="外部リンク編集" id="link">
</th>
<th>
<%request.getSession().setAttribute("statusList", statusList); %>
<input type="submit" name="states" value="ステータス編集" id="states">
</th>
<th>
<!-- 未実装のため id="phone"削除してます-->
<input type="button" name="phone" value="共有携帯電話編集">
</th>
</tr>
</Table>
<br>



<table  border=2px width =100%>
<caption>携帯電話の貸し出し一覧</caption>
<tr>
<th>
<div class="scr">
<p class="ontheBox">
<%int userMapping = 0;
boolean userFLG = false;
int teluser = 0;
int masteruser = 0;
for(int i = 0; i < telMap.length; i++){ %>


<%

userFLG = false;
for(int j = 0; j < b.length && userFLG == false; j++){

		try{
			 teluser =Integer.parseInt(telMap[i][1]);
			 masteruser =Integer.parseInt(b[j][0]);
		}catch(Exception e){
			teluser = 0;
		}

		if(teluser > masteruser || teluser < masteruser){

		}else{ userMapping = j;
			userFLG = true;}
		}
		if(userFLG){%>
			<%=telMap[i][0]+" " + b[userMapping][1] +"："+ telMap[i][2] %>
			<br>
 		<%}else{%>
			<%=telMap[i][0] +" "+ "現在貸し出しがされていません。" %>
			<br>
	<%}
}%>
	</div>
	</th>
	</tr>

</table>
</body>
</html>