<%@page import="java.util.List"%>
<%@page import="todo.vo.TodoValueObject"%>
<%@page import="todo.dao.TodoDAO"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<title></title>

<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script>
$(function(){
	$("#return").click(function(){
		window.close();
	});
});

</script>
</head>
<body>
<h1>伝言確認</h1>

<form action="createMessage" style="display: inline;">
<input type="hidden" name="employeeID" value=<%=request.getAttribute("employeeID")%>>
<input type="submit" name="新規作成" value="新規作成">
<input type="submit" name="戻る" value="戻る" id="return">
</form>
<br><br>

<%
List<TodoValueObject> messageList = (List<TodoValueObject>)(request.getAttribute("messageList"));
for(TodoValueObject vo : messageList ){
%>

<div style="border-style: solid ; border-width: 1px;">
<p>

<%
String f="★";
if(vo.isBookMark()){ %>
<%=f %>
<%}%>
<table>
<tr>
<th>
差出人：
</th>
<th style="float: left"><input type="text" name="" value="<%=vo.getFirstName() %>"/>
<form action="mail.jsp" style="display: inline;">
<!-- 返信画面に必要な情報を取得 -->
<input type="hidden" name="send" value="">
<input type="hidden" name="firstName" value="">
<input type="button" name="返信" value="返信">
</form>
<form action="messageFav" method="get" style="display: inline;">
<input type="hidden" name="messageID" value="<%=vo.getMessageID()%>">
<input type="hidden" name="bookmark" value="<%=vo.isBookMark()%>">
<input type="submit" name="お気に入り" value="お気に入り">
</form>
<form action="deleteMessage" method="post" style="display: inline;">
<input type="hidden" name="id" value="<%=vo.getMessageID()%>">
<input type="hidden" name="e_id" value="<%=request.getAttribute("employeeID")%>">
<input type="submit" name="削除" value="削除">
</form>
</th>
</tr>
<tr>
<th>件名：</th>
<th style="float: left"><input type="text" name="subject" value="<%=vo.getSubject()%>" size="20"></th>
</tr>
<tr>
<th>本文：</th>
<th><textarea rows="3" cols="50"><%=vo.getMessage() %></textarea></th>
</tr>
</table>
</p>
</div>
<br>
<%}%>
</body>
</html>