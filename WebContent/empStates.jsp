<%@page import="todo.dao.TodoDAO"%>
<%@page import="java.util.List"%>
<%@page import="todo.vo.TodoValueObject"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<%int emp =  Integer.parseInt(request.getParameter("Emp"));
TodoDAO dao = new TodoDAO();
TodoValueObject vo = dao.getDetail(emp);
%>
<body>
<h1>ステータス更新</h1>
<form action="statesUpdate" method="GET">
<table border="1">
	<tr>
		<th><%=vo.getFirstName() %></th>
		<th>
			<select name="states">
			<%List<TodoValueObject> sta = (List<TodoValueObject>)dao.getStatus(); 
			for(TodoValueObject sv : sta){%>
				<option value=<%=sv.getStatusID()%>><%=sv.getStatus() %></option>
			<%} %>
			</select>
		</th>
	</tr>

	</table>
	<input type="hidden" name="empID" value=<%=vo.getEmployeeID() %>>
	<input type="hidden" name="roomID" value=<%=vo.getRoomID() %>>
	<input type="submit" value="更新">
	</form>
</body>
<%dao.closeConnection(); %>
</html>