// ドラッグ＆ドロップ処理をする
window.addEventListener("load", function(){

	//pos = null;
	ids = [];
	sid = [];
	move = [];
	var itemList = document.querySelectorAll("#tables th");
	for(var i=0; i<itemList.length; i++){
		itemList[i].draggable = true;	// Safari, Chrome	

		// ドラッグ開始時に実行されるイベント
		itemList[i].addEventListener("dragstart",DragStartFunc);
		// ドロップしたときに実行されるイベント
		itemList[i].addEventListener("drop",DropFunc);
		// ドラッグ中にマウスカーソルを移動すると実行されるイベント
		itemList[i].addEventListener("drag",DragFunc);
		// ドラッグ終了時に実行されるイベント
		// itemList[i].addEventListener("dragend",DragEndFunc);
		// ドラッグ中に、マウスカーソルがエレメント上を移動すると実行されるイベント
		// itemList[i].addEventListener("dragover",DragOverFunc);
		
		/**dragenterなどのイベントは「ドラッグ要素がドロップ要素に入った時」ではなく
		「[ドラッグ中のマウス]がドロップ要素に入った時」の実行される**/
		itemList[i].addEventListener("dragenter", function(evt){
			evt.preventDefault();
			var moveid = evt.currentTarget.id.split("_");
			//if(sid[0] > moveid[0]) continue;
			move[0] = Number(sid[0]) + Number(moveid[0]);
			move[1] = Number(sid[1]) + Number(moveid[1]);
			console.log(move);
		}, true);
		
		itemList[i].addEventListener("dragleave", function(evt){
			evt.preventDefault();
		}, true);
		itemList[i].addEventListener("dragover", function(evt){
			evt.preventDefault();
		}, true);
	}
	
	function DropFunc(evt){
		
		//デフォルトの機能を無効化する
		evt.preventDefault();
		
		//ドロップ先エレメント
		var ele = document.getElementById(evt.target.id);
		//ドロップ元エレメント(結合セルの場合は左上のセル)
		var ele2 = document.getElementById(ids[0]);
		//ドロップ元の幅
		var col = evt.dataTransfer.getData("col");
		
		//ドロップ元が結合されているセルだったら
		if(col > 1){

			/////ドロップ元の結合を解除して、空いたところに要素を足す/////
			var th = document.createElement("th");
			th.width = 100;
			console.log("col:" + col);
			$("#" + ids[0]).attr("colSpan","1");
			for(var i = 1;i < col;i++){
				th.id = ids[i];
				$("#" + ids[i - 1]).after(th);
			}
			///////////////////////////////////////////////
			
			////////////ドロップ先を結合する////////////
			
			/**var zahyou = evt.target.id.split("_");
			//右側をクリックしてたら左側を結合または逆
			console.log(pos);
			if(evt.dataTransfer.getData("pos") == "left"){
				var id = zahyou[0] + "," + zahyou[1] + 1;
				console.log("結合id" + id + "と" + evt.target.id);
				var ele3 = document.getElementById(id);
				var tr = ele.parentNode;
				var th = tr.getElementsByTagName("th")[zahyou[1] + 1];
				ele.conSpan = "2";
				//tr.removeChild(th);
				
			}else if(evt.dataTransfer.getData("pos") == "right"){
				console.log(zahyou[1] - 1 + "," + zahyou[0]);
				//結合するid
				zahyou[1] = zahyou[1] - 1;
				console.log("結合id" + zahyou + "と" + evt.target.id);
				//結合するエレメント
				var ele3 = document.getElementById(zahyou);
				var tr = ele.parentNode;
				var th = tr.getElementsByTagName("th")[zahyou[1] - 1];
				ele.colSpan = "2";
				//console.log("ele3:" + ele3);
				//console.log("th:" + th.id);
				tr.removeChild(th);
			}**/
			//////////////////////////////////////

		}
		
		ele.innerHTML = evt.dataTransfer.getData("text");
		ele2.innerHTML = "";
	}
	
	function DragStartFunc(evt){
	
		var col = evt.currentTarget.colSpan;
		var row = evt.currentTarget.rowSpan;
		evt.dataTransfer.setData("text", evt.currentTarget.innerHTML);
		evt.dataTransfer.setData("col", col);
		evt.dataTransfer.setData("row", row);
	
		//クリックされた座標
		sid = evt.target.id.split("_");
		
		var index = 0;
		for(var y = sid[0];y < col;y++){
			for(var x = sid[1];x < row;x++){
				var str = x + "_" + y;
				console.log("str:" + str);
				ids[index] = str;
				index++;
			};
		}
		
		if(evt.currentTarget.colSpan > 1){
			var rect = evt.target.getBoundingClientRect();
			var mouseX = evt.clientX - rect.left;
			//var mouseY = evt.clientY - rect.top;
			var width = evt.currentTarget.offsetWidth;
			if(mouseX > width / 2){
				pos = "right";
			}else if (mouseX < width /2){
				pos = "left";
			}
			console.log(pos + "側を押しました");
		}
	}
	
	function DragFunc(evt){	}
	
}, true);
