<%@page import="java.util.List"%>
<%@page import="todo.vo.TodoValueObject"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script>
function checkIP(flag) {
	console.log("ViewIP:" + flag);
	if (flag) {
		document.getElementById("ipTrue").checked = true;
	}else{
		document.getElementById("ipFalse").checked = true;
	}
}

function checkID(flag) {
	console.log("ViewID:" + flag);
	if (flag) {
		document.getElementById("idTrue").checked = true;
	}else{
		document.getElementById("idFalse").checked = true;
	}
}

</script>
</head>
<%TodoValueObject vo =  (TodoValueObject)(request.getAttribute("vo"));%>
<body onload="checkIP(<%=vo.isViewIP()%>);checkID(<%=vo.isViewID()%>)">
<h1>社員情報更新</h1>
<form action="register" method="GET">
<table border="1">
	<tr>
		<th>苗字</th>
		<td>
			<input type="text" name="firstName" size="20" value=<%=vo.getFirstName() %>>
		</td>
	</tr>
	<tr>
		<th>名前</th>
		<td>
			<input type="text" name="lastName" size="20" value=<%=vo.getLastName() %>>
		</td>
	</tr>
	<tr>
		<th>苗字(かな)</th>
		<td>
			<input type="text" name="firstKana" size="20" value=<%=vo.getFirstKana() %>>
		</td>
	</tr>
	<tr>
		<th>名前(かな)</th>
		<td>
			<input type="text" name="lastKana" size="20" value=<%=vo.getLastKana() %>>
		</td>
	</tr>
	<tr>
		<th>IPアドレス</th>
		<td>
			<input type="text" name="ipaddress" size="20" value=<%=vo.getIpAddress() %>>
		</td>
	</tr>
	<tr>
		<th>内線番号</th>
		<td>
			<input type="text" name="address" size="20" value=<%=vo.getLocalPhoneNumber() %>>
		</td>
	</tr>
	
	<tr>
		<th>部屋</th>
		<td>
			<select name="roomSelect">
			<%System.out.println("room:" + vo.getRoomID()); %>
			<%for(TodoValueObject room : (List<TodoValueObject>)request.getAttribute("room") ){
				// 登録されている部署をプルダウンメニューの初期値に設定
				if(room.getRoomID() == vo.getRoomID()){%>
					<option selected="selected" value=<%=room.getRoomID()%>><%=room.getRoomName() %></option>
				<%}else{%>
					<option value=<%=room.getRoomID() %>><%=room.getRoomName() %></option>
				<%}%>
			<%}%>
			</select>
		</td>
	</tr>
	
	<tr>
		<th>IPアドレスの表示</th>
		<td>
			<input type="radio" name="viewIP" id="ipTrue" value="true"/>表示
			<input type="radio" name="viewIP" id="ipFalse" value="false"/>非表示
		</td>
	</tr>
	<tr>
		<th>社員Noの表示</th>
		<td>
			<input type="radio" name="viewID" id="idTrue"value="true"/>表示
			<input type="radio" name="viewID" id="idFalse"value="false"/>非表示
		</td>
	</tr>
	<tr>
	<th>メモ</th>
	<th>
			<textarea></textarea>
	</th>
	</tr>
	</table>
	<input type="hidden" name="employeeID" value=<%=vo.getEmployeeID() %>>
	<input type="hidden" name="statesID" value=0>
	<input type="hidden" name="states" value="null">
	<input type="hidden" name="memo" value="null">
	<input type="submit" value="更新">
	</form>
</body>
</html>