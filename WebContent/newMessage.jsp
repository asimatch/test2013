<%@page import="java.util.List"%>
<%@page import="todo.vo.TodoValueObject"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<%TodoValueObject vo = (TodoValueObject)request.getAttribute("detail"); %>
<% %>
<body>
	<form method="get" action="registerMessage">
		<table border="1">
			<tr>
				<td>タイトル</td>
				<td><textarea cols=40 rows=1 name="title" style="overflow: hidden;"></textarea></td>
			</tr>
			<tr>
				<td>内容</td>
				<td><textarea cols=40 rows=5 name="message"></textarea></td>
			</tr>
			<tr>
				<td>記入者</td>
				<td><textarea cols=40 rows=1 name="fromName" style="overflow: hidden;"><%=vo.getFirstName() + " " + vo.getLastName()%></textarea></td>
			</tr>
			<tr>
				<td>返信用メールアドレス(宛先)</td>
				<td><textarea cols=40 rows=1 name="reAddr" style="overflow: hidden;">2341007@bemax.jp</textarea></td>
			</tr>
		</table>
		メッセージの送信先:<br>
		<%List<TodoValueObject> empDetail = (List<TodoValueObject>)request.getAttribute("empDetail"); 
		for(TodoValueObject vo2 : empDetail){%>
			<input type="checkbox" name="send" value=<%=vo2.getContact() %>><%=vo2.getContact() %><br>
		<%} %>
		<input type="submit" value="送信">
		<input type="hidden" name="messageID" value="null">
		<input type="hidden" name="employeeID" value=<%=vo.getEmployeeID() %>>
		<input type="hidden" name="address" value=<%=vo.getContact() %>>
		<input type="hidden" name="ipAddr" value=<%=vo.getIpAddress() %>>
	</form>
</body>
</html>