<%@page import="todo.vo.TodoValueObject"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<title></title>

</head>
<body>
<%TodoValueObject vo = (TodoValueObject)(request.getAttribute("vo")); %>
<form id="sender" action="register" method="POST">
<table>
	<tr>
		<th>氏　名=</th>
		<td><%=vo.getFirstName()%>　</td>
		<td><%=vo.getLastName()%></td>
	</tr>
	<tr>
		<th>氏　名（カナ）=</th>
		<td><%=vo.getFirstKana()%>　</td>
		<td><%=vo.getLastKana()%></td>
	</tr>
	<tr>
		<th>内　線=</th>
		<td><%=vo.getLocalPhoneNumber()%></td>
	</tr>
	<tr>
		<th>状　態=</th>
		<td><%=vo.getStatus()%></td>
	</tr>
	<tr>
		<th>行　先=</th>
		<td><%=vo.getDestination()%></td>
	</tr>
	<tr>
		<th>開　始=</th>
		<td><%=vo.getStartTime()%></td>
	</tr>
	<tr>
		<th>終　了=</th>
		<td><%=vo.getEndTime()%></td>
	</tr>
	<tr>
		<th>直　行=</th>
		<td><%=vo.isNeFLG()%></td>
	</tr>
	<tr>
		<th>直　帰=</th>
		<td><%=vo.isNrFLG()%></td>
	</tr>
	<tr>
		<th>連絡先=</th>
		<td>内容未定</td>
	</tr>
	<tr>
		<th>更　新=</th>
		<td><%=vo.getLastUpdate()%></td>
	</tr>
	<tr>
		<th>メールアドレス=</th>
		<td><%=vo.getContact()%></td>
	</tr>
	<%if(vo.isViewID()){ %>
	<tr>
		<th>社員ID=</th>
		<td><%=vo.getEmployeeID()%></td>
	</tr>
	<%} %>
	<%if(vo.isViewIP()){ %>
	<tr>
		<th>IPアドレス=</th>
		<td><%=vo.getIpAddress()%></td>
	</tr>
	<%} %>
	<tr>
		<th>-----メモ-----</th>
	</tr>
	<tr>
		<td><%=vo.getMemo() %></td>
	</tr>
</table>
</form>
</body>
</html>