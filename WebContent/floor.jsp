<%@page import="todo.vo.TodoValueObject"%>
<%@page import="java.util.List"%>
<%@page import="todo.dao.TodoDAO"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>フロア登録・編集・削除</title>

<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script>
$(function(){
	$("#register").click(function(){

	});
	$("#edit").click(function(){
		$("#comp").text("編集が完了しました");
	});
});

function(str){}


</script>
</head>
<body>
<h1 align="center">フロア編集</h1>
<div class="floor"></div>


<form action="registerRoom" method="get">
<input type="hidden" name="roomID" value=0>
部屋名<input type="text" name="roomName">
部署選択<select name="postSelect">
<%
TodoDAO dao = new TodoDAO();
List<TodoValueObject> roomList = dao.getRoomAll();
List<TodoValueObject> departmentList = dao.getDepartment();
for(TodoValueObject vo : departmentList ){
%>
<option value=<%=vo.getDepartmentID()%>><%=vo.getDepartmentName()%></option>
<%}%>
</select><br>

縦の大きさ<input type="text" name="height"size="3">
横の大きさ<input type="text" name="width" size="3"><br>

縦の大きさ(2)<select name="height2">
<option>1</option>
<option>2</option>
<option>3</option>
<option>4</option>
<option>5</option>
<option>6</option>
<option>7</option>
<option>8</option>
<option>9</option>
<option>10</option>
</select>
横の大きさ(2)<select name="width2">
<option>1</option>
<option>2</option>
<option>3</option>
<option>4</option>
<option>5</option>
<option>6</option>
<option>7</option>
<option>8</option>
<option>9</option>
<option>10</option>
</select>
<br>
<input type="submit" name="登録" value="登録" id="register" style="float: left">
</form>
<input type="submit" name="戻る" value="戻る" id="retrun">


<br><br><br>
<%for(TodoValueObject vo : roomList){ %>
<form action="registerRoom" method="get">
<input type="hidden" name="roomID" value=<%=vo.getRoomID() %>>
<input type="hidden" name="width2" value=<%=vo.getWidth() %>>
<input type="hidden" name="height2" value=<%=vo.getHeight() %>>
部屋名<input type="text" name="roomName"value=<%=vo.getRoomName() %>><br>

部署<select name="postSelect">
<%for(TodoValueObject vo2 : departmentList ){
	// 登録されている部署をプルダウンメニューの初期値に設定
	if(vo.getDepartmentName().equals(vo2.getDepartmentName())){%>
		<option selected="selected" value=<%=vo2.getDepartmentID()%>><%=vo2.getDepartmentName()%></option>
	<%}else{%>
		<option value=<%=vo2.getDepartmentID()%>><%=vo2.getDepartmentName()%></option>
	<%}%>
<%}%>
</select>

<table border="1" width="100px" height="100px">
<%for(int i = 0;i < vo.getHeight();i++){%>
	<tr>
	<%for(int j = 0 ;j < vo.getWidth();j++){%>
	<td></td>
	<%}%>
	</tr>
<%}%>
</table>
<input type="submit" name="編集" value="編集" id="edit" style="float: left">
</form>
<form action="deleteRoom" method="post">
<input type="hidden" name="id" value=<%=vo.getRoomID() %>>
<input type="submit" name="削除" value="削除" id="delete"><br>
</form>
<%}%>

<br><br><br><br><br>

<br><br>
<div id="comp"></div>

</body>
<%dao.closeConnection();%>
</html>