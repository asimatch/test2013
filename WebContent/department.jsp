<%@page import="todo.vo.TodoValueObject"%>
<%@page import="java.util.List"%>
<%@page import="todo.dao.TodoDAO"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>

<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script>
$(function(){
	$("#return").click(function(){
		window.close();
	});
});
</script>
</head>
<body>
<H2>部署編集</H2>

<form method="get" action="registerDepartment" style="float:left">
<input type="hidden" name="departmentID" value=0>
部署名<input type="text" name="departmentName">
<input type="submit" value="登録">
</form>
<input type="button" id="return" value="戻る">
<br><br>

<%
TodoDAO dao = new TodoDAO();
List<TodoValueObject> departmentList = dao.getDepartment();
for(TodoValueObject vo : departmentList ){
%>

<table border=1>
<tr>
<td>
<form action="deleteDepartment" method="post">
<input type="hidden" name="id" value=<%=vo.getDepartmentID()%>>
<input type="submit" name="delete" value="削除">
</form>
</td>
<td>
部署名
</td>
<td>
<%=vo.getDepartmentName() %>
</td>
<tr>
</table>
<%} %>
</body>
<%dao.closeConnection();%>
</html>