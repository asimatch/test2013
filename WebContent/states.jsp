<%@page import="todo.dao.TodoDAO"%>
<%@page import="todo.vo.TodoValueObject"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<title></title>

<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script>
$(function(){
	$(".colorselect").click(function(){
		var text = $(this).name(".cs");
		$(".c").val("text");
	});
	$("#return").click(function(){
		window.close();
	});
});


function push(str){
	document.c.co.value = str;
}

function setUpdate(id, data){
	var test = id.firstElementChild.value;
	var test2 = id.firstElementChild.nextElementSibling.value;
	var test3 = id.firstElementChild.nextElementSibling.nextElementSibling.value;

	data[0].value = test;
	data[1].value = test2;
	data[2].value = test3;
}

</script>
</head>
<body>

<form name="c" action="registerStatus">
<h2>色の追加</h2>
<input type="hidden" name="flag" value="insert">
ステータスID：<input type="text" name="statusID"/><br>
ステータス名：<input type="text" name="status"/><br>
RGBの色コード：<input type="text" name="color" value="" id="co"/><br>
色一覧<br>

<div class="cs">
<input type="button" name="#ff0000" onclick="push(this.name)" style="background-color: #ff0000"/>
<input type="button" name="#ff8000" onclick="push(this.name)" style="background-color: #ff8000"/>
<input type="button" name="#ffff00" onclick="push(this.name)" style="background-color: #ffff00"/>
<input type="button" name="#80ff00" onclick="push(this.name)" style="background-color: #80ff00"/>
<input type="button" name="#00ff00" onclick="push(this.name)" style="background-color: #00ff00"/>
<input type="button" name="#00ff80" onclick="push(this.name)" style="background-color: #00ff80"/>
<input type="button" name="#00ffff" onclick="push(this.name)" style="background-color: #00ffff"/>
<input type="button" name="#00ffff" onclick="push(this.name)" style="background-color: #0000ff"/>
<input type="button" name="#008080" onclick="push(this.name)" style="background-color: #008080"/>
<input type="button" name="#0080ff" onclick="push(this.name)" style="background-color: #0080ff"/>
<input type="button" name="#0000ff" onclick="push(this.name)" style="background-color: #0000ff"/>
<input type="button" name="#8000ff" onclick="push(this.name)" style="background-color: #8000ff"/>
<input type="button" name="#800000" onclick="push(this.name)" style="background-color: #800000"/>
<input type="button" name="#808080" onclick="push(this.name)" style="background-color: #808080"/>
<input type="button" name="#808000" onclick="push(this.name)" style="background-color: #808000"/>
<input type="button" name="#ff00ff" onclick="push(this.name)" style="background-color: #ff00ff"/>
</div>
<input type="submit" name="追加" value="追加" style="float: left">
<input type="reset" name="削除" value="リセット">
</form>

<input type="submit" name="戻る" value="戻る" id="return" style="clear:left"><br><br>

<%
/**int[] statesID = {1,2,3,4,5};
String[] statesName = {"出席","欠席","遅延","公欠","出席停止"};
String[] color ={"#ff0000","#ff8000","#ffff00","#80ff00","#00ffff"};*/
	
	TodoDAO dao = new TodoDAO();
	List<TodoValueObject> list = (List<TodoValueObject>)dao.getStatus();
	for(TodoValueObject vo : list){
%>



<table border="1">
<tr>
<td>

<div id="table<%=vo.getStatusID()%>">
ステータスID：<input type="text" name="t_statusID" value="<%=vo.getStatusID()%>" style="background-color:<%=vo.getColor() %>" />
ステータス名：<input type="text" name="t_status" value="<%=vo.getStatus() %>" style="background-color:<%=vo.getColor() %>" />
RGBの色コード：<input type="text" name="t_color" value="<%=vo.getColor() %>" style="background-color:<%=vo.getColor() %>" /><br>
</div>

<form action="deleteStatus" method="post">
<input type="hidden" name="id" value="<%=vo.getStatusID() %>">
<input type="submit" name="削除" value="削除" style="float: left">
</form>

<form name="data<%=vo.getStatusID()%>" action="registerStatus" method="get">
<input type="hidden" name="statusID" value="" />
<input type="hidden" name="status" value="" />
<input type="hidden" name="color" value="" />
<input type="hidden" name="id" value=<%=vo.getStatusID()%> />
<input type="hidden" name="flag" value="update" />
<input type="submit" name="編集" value="編集" onclick="setUpdate(table<%=vo.getStatusID()%>, data<%=vo.getStatusID()%>)">
</form>
</td>
</tr>
</table>
<br>
<%}%>
</body>
<%dao.closeConnection();%>
</html>