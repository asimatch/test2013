<%@page import="java.util.List"%>
<%@page import="todo.vo.TodoValueObject"%>
<%@page import="todo.dao.TodoDAO"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>外部リンク</title>

<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script>
$(function(){
	$("#return").click(function(){
		window.close();
	});
});

// 編集時に入力された値をformに渡す
function change(id){
	var ele = document.getElementById("updateForm" + id);
	var edit = document.getElementById("edit" + id);
	var url = document.getElementById("url" + id);
	ele.edit.value = edit.value;
	ele.url.value = url.value;
}

// 表示順変更の値を代入
function upDown(id, name){
	var move = document.getElementById("move" + id);
	if(name == "up"){
		move.value = 1;
	}else{
		move.value = -1;
	}
}

</script>
</head>
<body>
<h1>外部リンク</h1>

<h4>リンク登録</h4>

<%
int roomID = (Integer)(request.getSession().getAttribute("roomID"));
/**String[] num = {"zero","one","two"};
String[] pagename = {"ヤフージャパン","グーグル","ビーマックス"};
String[] url = {"http://yahoo.co.jp","http://google.co,jp","http://bemax.co.jp"};*/
%>
<form method="get" action="registerLink">
<input type="hidden" name="linkID" value=0>
ページ名<input type="text" name="edit" ><br><br>
URL<input type="text" name="url"><br><br>
<input type="hidden" name="roomID" value=<%=roomID%>>
<input type="submit" value="登録" style="float: left">
</form>

<input type="button" id="return"value="戻る">
<br><br><br>


<table border="1" height="10">
<tr><th>ページ名</th><th>URL</th><th></th></tr>

<%
TodoDAO dao = new TodoDAO();
List<TodoValueObject> linkList = dao.getLink(roomID);
for(int i = 0;i < linkList.size();i++){
	TodoValueObject vo = linkList.get(i);
	%>
	<tr>
		<td>
			<input type="text" id="edit<%=vo.getLinkID()%>" value="<%=vo.getPageName() %>"/>
		</td>
		<td>
			<input type="text" id="url<%=vo.getLinkID()%>" value="<%=vo.getLinkURL() %>"/>
		</td>
		<td>
		<form action="registerLink" method="get" id="updateForm<%=vo.getLinkID()%>" style="display: inline;">
			<%if(i == 0){%>
				<input type="button" value="↑">
			<%}else{ %>
			<!-- こちらが本来submit -->
				<input type="submit" name="up" value="↑" onclick="upDown(<%=vo.getLinkID()%>, this.name)">
			<%} %>
			<%if(i == linkList.size() - 1){ %>
				<input type="button" value="↓">
			<%}else{ %>
			<!-- こちらが本来submit -->
				<input type="submit" id="down<%=vo.getLinkID()%>" name="down" value="↓" onclick="upDown(<%=vo.getLinkID()%>, this.name)">
			<%} %>
			<input type="hidden" name="move" id="move<%=vo.getLinkID()%>">
			<input type="hidden" name="linkID" value=<%=vo.getLinkID() %>>
			<input type="hidden" name="edit" value=<%=vo.getPageName() %>>
			<input type="hidden" name="url" value=<%=vo.getLinkURL() %>>
			<input type="hidden" name="roomID" value=<%=roomID%>>
			<input type="hidden" name="num" value=<%=vo.getNum() %>>
			<input type="submit" value="編集" onclick="change(<%=vo.getLinkID()%>)">
		</form>

			<form action="deleteLink" method="post" style="display: inline;">
				<input type="hidden" name="id" value=<%=vo.getLinkID() %>>
				<input type="submit" value="削除" style="display: inline-block;">
			</form>
		</td>
	</tr>
<%}%>

</table>
</body>
<%dao.closeConnection();%>
</html>