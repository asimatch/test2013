<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員登録</title>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script>

</script>
</head>
<body>
<h1>社員登録</h1>

<form method="get" action="register">
<table border="1" width="500" height="300">
<tr>
<th>苗字</th>
<th><input type="text" name="firstName" >（全角）</th>
</tr>
<tr>
<th>名前</th>
<th><input type="text" name="lastName" >（全角）</th>
</tr>
<tr>
<th>カナ</th>
<th><input type="text" name="firstKana">（全角カナ）</th>
</tr>
<tr>
<th>カナ</th>
<th><input type="text" name="lastKana">（全角カナ）</th>
</tr>
<tr>
<th>内線番号</th>
<th><input type="text" name="extention">(半角数字）</th>
</tr>
<tr>
<th>ステータス</th>
<th>
<select name="states">
<option value="1">出席</option>
<option value="2">欠席</option>
<option value="3">離席</option>
</select>
</th>
</tr>
<tr>
<th>連絡先</th>
<th><input type="text" name="address"></th>
</tr>
<tr>
<th>社員番号</th>
<th><input type="text" name="employeeID">（半角数字）</th>
</tr>
<tr>
<th>IPアドレス</th>
<th><input type="text" name="ipaddress">（半角数字）</th>
</tr>
<tr>
<th>メモ</th>
<th><input type="text" name="memo" size="20" ></th>
</tr>
</table>
<br><br>
<input type="hidden" name="viewIP" value="false">
<input type="hidden" name="viewIP" value="false">
<input type="hidden" name="statesID" value="null">
<input type="hidden" name="roomID" value="null">
<input type="submit" name="edit" value="登録">
<input type="submit" name="return" value="戻る">
</form>
</body>
</html>