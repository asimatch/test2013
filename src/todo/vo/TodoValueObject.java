package todo.vo;

import java.sql.Date;
import java.sql.Time;

/**
 * TODO検索結果１行単位のValueObject
 *
 */
public class TodoValueObject {

	/** 社員No */
    private int employeeID;
	
	/** 苗字 */
    private String firstName;

    /** 名前 */
    private String lastName;

    /** IPAddress */
    private String ipAddress;

    /** IPAddressの表示フラグ */
    private boolean viewIP;

    /** 社員No表示フラグ */
    private boolean viewID;

    /** 内線番号 */
    private String localPhoneNumber;
    
    /** ステータスID */
    private int statusID;
    
    /** 更新時間 */
    private Date lastUpdate;
    
    /** 所属部屋 */
    private int roomID;
    
    /** 備考欄 */
    private String message;
    
    /** 左上のX座標 */
    private int leftX;
    
    /** 左上のY座標 */
    private int leftY;
    
    /** 右上のX座標 */
    private int rightX;
    
    /** 右上のY座標 */
    private int rightY;
    
    /** 行先名 */
    private String destination;
    
    /** 開始時間 */
    private Time startTime;
    
    /** 終了時間 */
    private Time endTime;
    
    /** 直行フラグ */
    private boolean neFLG;
        
	/** 直帰フラグ */
    private boolean nrFLG;

    /** メールアドレスor携帯番号 */
    private String contact;
    
    /** 連絡先の種類 */
    private String detail;
    
    /** 貸出電話ID */
    private int phoneID;
    
    /** 持ち出し先等のメモ */
    private String memo;
    
    /** ステータス名 */
    private String status;
    
    /** RGB12桁の色コード */
    private String color;
    
    /** 部屋名 */
    private String roomName;
    
    /** 部屋の横幅 */
    private int width;
    
    /** 部屋の縦幅 */
    private int height;
    
    /** メッセージのID */
    private int bulletinID;
    
    /** メッセージの内容 */
    private String bulletin;
    
    /** メッセージID */
    private int messageID;

    /** 送信者IPアドレス */
    private String sendIP;
    
    /** 送信先のID */
    private int ReceiveID;

    /** 送信時間 */
    private Time Time;
    
    /** お気に入りフラグ */
    private boolean BookMark;
    
    /** 既読 */
    private boolean ReadFLG;

    /** 電話番号 */
    private String TelNo;
    
    /** 配列の横位置 */
    private int TableX;
    
    /** 配列の縦位置 */
    private int TableY;
    
    /** 掲示板の投稿日 */
    private Date Update;
    
    /** 投稿者のIPアドレス */
    private String B_IPAddress;
    
    /** 連絡先種類 */
    private String type;
    
    /** 貸出携帯電話の状態 */
    private String phoneStatus;
    
    /** 部署ID */
    private int departmentID;
    
    /** 部署名 */
    private String departmentName;
    
    /** 苗字(カナ) */
    private String firstKana;
    
    /** 名前(カナ) */
    private String lastKana;
    
    /** リンクの表示順 */
    private int num;
    
    /** リンク名 */
    private String pageName;
    
    /** リンクURL */
    private String linkURL;
    
    /** リンクID */
    private int linkID;
    
    /** 件名 */
    private String subject;
    
    /** アドレスキー */
    private int detailID;
    
	public int getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public boolean isViewIP() {
		return viewIP;
	}

	public void setViewIP(boolean viewIP) {
		this.viewIP = viewIP;
	}

	public boolean isViewID() {
		return viewID;
	}

	public void setViewID(boolean viewID) {
		this.viewID = viewID;
	}

	public String getLocalPhoneNumber() {
		return localPhoneNumber;
	}

	public void setLocalPhoneNumber(String localPhoneNumber) {
		this.localPhoneNumber = localPhoneNumber;
	}

	public int getStatusID() {
		return statusID;
	}

	public void setStatusID(int statusID) {
		this.statusID = statusID;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public int getRoomID() {
		return roomID;
	}

	public void setRoomID(int roomID) {
		this.roomID = roomID;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getLeftX() {
		return leftX;
	}

	public void setLeftX(int leftX) {
		this.leftX = leftX;
	}

	public int getLeftY() {
		return leftY;
	}

	public void setLeftY(int leftY) {
		this.leftY = leftY;
	}

	public int getRightX() {
		return rightX;
	}

	public void setRightX(int rightX) {
		this.rightX = rightX;
	}

	public int getRightY() {
		return rightY;
	}

	public void setRightY(int rightY) {
		this.rightY = rightY;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Time getStartTime() {
		return startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}

	public Time getEndTime() {
		return endTime;
	}

	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}

	public boolean isNeFLG() {
		return neFLG;
	}

	public void setNeFLG(boolean neFLG) {
		this.neFLG = neFLG;
	}

	public boolean isNrFLG() {
		return nrFLG;
	}

	public void setNrFLG(boolean nrFLG) {
		this.nrFLG = nrFLG;
	}
	
	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public int getPhoneID() {
		return phoneID;
	}

	public void setPhoneID(int phoneID) {
		this.phoneID = phoneID;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getBulletinID() {
		return bulletinID;
	}

	public void setBulletinID(int bulletinID) {
		this.bulletinID = bulletinID;
	}

	public String getBulletin() {
		return bulletin;
	}

	public void setBulletin(String bulletin) {
		this.bulletin = bulletin;
	}

	public int getMessageID() {
		return messageID;
	}

	public void setMessageID(int messageID) {
		this.messageID = messageID;
	}

	public String getSendIP() {
		return sendIP;
	}

	public void setSendIP(String sendIP) {
		this.sendIP = sendIP;
	}

	public int getReceiveID() {
		return ReceiveID;
	}

	public void setReceiveID(int receiveID) {
		ReceiveID = receiveID;
	}

	public Time getTime() {
		return Time;
	}

	public void setTime(Time time) {
		Time = time;
	}

	public boolean isBookMark() {
		return BookMark;
	}

	public void setBookMark(boolean bookMark) {
		BookMark = bookMark;
	}

	public boolean isReadFLG() {
		return ReadFLG;
	}

	public void setReadFLG(boolean readFLG) {
		ReadFLG = readFLG;
	}

	public String getTelNo() {
		return TelNo;
	}

	public void setTelNo(String telNo) {
		TelNo = telNo;
	}
	
	public int getTableX() {
		return TableX;
	}

	public void setTableX(int tableX) {
		TableX = tableX;
	}

	public int getTableY() {
		return TableY;
	}

	public void setTableY(int tableY) {
		TableY = tableY;
	}
	
	public Date getUpdate() {
		return Update;
	}

	public void setUpdate(Date update) {
		Update = update;
	}

	public String getB_IPAddress() {
		return B_IPAddress;
	}

	public void setB_IPAddress(String b_IPAddress) {
		B_IPAddress = b_IPAddress;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPhoneStatus() {
		return phoneStatus;
	}

	public void setPhoneStatus(String phoneStatus) {
		this.phoneStatus = phoneStatus;
	}

	public int getDepartmentID() {
		return departmentID;
	}

	public void setDepartmentID(int departmentID) {
		this.departmentID = departmentID;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getFirstKana() {
		return firstKana;
	}

	public void setFirstKana(String firstKana) {
		this.firstKana = firstKana;
	}

	public String getLastKana() {
		return lastKana;
	}

	public void setLastKana(String lastKana) {
		this.lastKana = lastKana;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	public String getLinkURL() {
		return linkURL;
	}

	public void setLinkURL(String linkURL) {
		this.linkURL = linkURL;
	}

	public int getLinkID() {
		return linkID;
	}

	public void setLinkID(int linkID) {
		this.linkID = linkID;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getDetailID() {
		return detailID;
	}

	public void setDetailID(int detailID) {
		this.detailID = detailID;
	}
	
}
