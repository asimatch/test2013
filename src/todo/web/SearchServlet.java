package todo.web;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import todo.dao.TodoDAO;
import todo.vo.TodoValueObject;

/**
 * 検索機能。タスク一覧を取得し、一覧結果へフォワードする。
 */
@WebServlet( urlPatterns={"/search"})
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// voの作成
		TodoValueObject vo = new TodoValueObject();

		// タスク１件のvoをリクエスト属性へバインド
		request.setAttribute("vo", vo);
		
		// DAOの取得
		TodoDAO dao = new TodoDAO();
		
		// 表示する部屋ID
		int roomID = 1;
	
		// roomID取得
		if(request.getParameter("pull") != null){
			roomID = Integer.parseInt(request.getParameter("pull"));
		//　初期表示はIPアドレスで部屋検索
		}/**else{
			//IPアドレスの取得
			try{
				InetAddress iAddr = InetAddress.getLocalHost();
				String addr = iAddr.getHostAddress();
				// アドレスで部屋IDを取得
				//roomID = dao.getRoomID(addr);
			}catch (Exception e) {
				e.printStackTrace();
			}
		}*/
			
		try {
			// 社員一覧の取得
			List<TodoValueObject> list = dao.todoList(roomID);
			request.setAttribute("empList", list);
			
			// ステータス情報の取得
			List<TodoValueObject> status = dao.getStatus();
			request.setAttribute("status", status);
			
			// すべての部屋を取得
			List<TodoValueObject> roomAll = dao.getRoom();
			request.setAttribute("room", roomAll);

			// 表示する部屋情報の取得
			TodoValueObject room = dao.getRoom(roomID);
			request.setAttribute("roomInfo", room);

			// 既読フラグの取得
			ArrayList<Integer> read = dao.getReadFLG(roomID);
			request.setAttribute("readFLG", read);

			// 掲示板の取得
			List<TodoValueObject> bulletin = dao.getBulletin();
			request.setAttribute("bulletin", bulletin);

			// 連絡先（持ち出し携帯電話）の取得
			List<TodoValueObject> rentalPhone = dao.getRentalPhone();
			request.setAttribute("rentalPhone", rentalPhone);

		} catch (Exception e) {
			throw new ServletException(e);
		} finally {
			// DAOの処理が完了したら接続を閉じる
			dao.closeConnection();
		}

		RequestDispatcher rd = request.getRequestDispatcher("/index2.jsp");
		rd.forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	        throws ServletException, IOException {
	    doGet(request, response);
	}
}
