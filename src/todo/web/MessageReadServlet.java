package todo.web;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import todo.dao.TodoDAO;
import todo.vo.TodoValueObject;

/**
 * 登録処理を行う。
 */
@WebServlet("/messageRead")
public class MessageReadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		// リクエストパラメータを受け取り、更新VOに格納する準備をする
		String id = request.getParameter("num");
		String empStr = request.getSession().getAttribute("EmployeeID" + id).toString();
		int employeeID = Integer.parseInt(empStr);
		
		// DAOの取得
		TodoDAO dao = new TodoDAO();

		try {
			dao.checkMessage(employeeID);
			List<TodoValueObject> messageList = dao.getMessage();
			request.setAttribute("messageList", messageList);
			request.setAttribute("employeeID", employeeID);
		} catch (Exception e) {
			throw new ServletException(e);
		} finally {
			// DAOの処理が完了したら接続を閉じる
			dao.closeConnection();
		}

		// 登録完了→一覧画面を表示する
		RequestDispatcher rd = request.getRequestDispatcher("/message.jsp");
		rd.forward(request, response);
	}

}
