package todo.web;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import todo.dao.TodoDAO;
import todo.vo.TodoValueObject;

/**
 * 登録処理を行う。
 */
@WebServlet("/registerPhone")
public class RegisterPhoneServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータを受け取り、更新VOに格納する準備をする
		int phoneID = Integer.parseInt(request.getParameter("phoneID"));
		//String kind = new String (request.getParameter("kind").getBytes("ISO-8859-1"));
		String contact = new String (request.getParameter("contact").getBytes("ISO-8859-1"));
		String status = new String (request.getParameter("status").getBytes("ISO-8859-1"));
		String memo = new String (request.getParameter("memo").getBytes("ISO-8859-1"));

	    
		// VOへ格納する。
		TodoValueObject vo = new TodoValueObject();
		vo.setTelNo(contact);
		//vo.setType(kind);
		vo.setPhoneStatus(status);
		vo.setMemo(memo);

		// 入力チェックを行う。
		//boolean checkResult = vo.valueCheck();

		// もし入力チェックエラーがあった場合は、エラーメッセージを表示し、再入力させるため元の詳細画面へ戻る
		/**if (! checkResult ) {
			request.setAttribute("errorMessages", vo.getErrorMessages());
			// タスク１件のvoをリクエスト属性へバインド
			request.setAttribute("vo", vo);

			// 詳細画面を表示する
			RequestDispatcher rd = request.getRequestDispatcher("/detail.jsp");
			rd.forward(request, response);
			return;
		}**/

		// DAOの取得
		TodoDAO dao = new TodoDAO();
		//String message = "";
		try {
			// 更新または登録処理を行う
			if (phoneID == 0) {
				dao.insertPhone(vo);
				List<TodoValueObject> phoneList = dao.getRentalPhone();
				request.getSession().setAttribute("phoneList", phoneList);
				//message = "タスクの新規登録処理が完了しました。";
				//setMessage(request, message);
			//} else {
				//dao.registerUpdate(vo);
				//message = "タスク[ " + id + " ]の更新処理が完了しました。";
				//setMessage(request, message);
			}
		} catch (Exception e) {
			throw new ServletException(e);
		} finally {
			// DAOの処理が完了したら接続を閉じる
			dao.closeConnection();
		}

		/*
		String toAddr = "toaddr@example.com";
		String fromAddr = "fromaddr@example.com";
		String personName = "Mail Test User";
		String subject = "TODO管理アプリケーションからの報告です";
		// 完了時にメールを送信する
		SimpleMailSender mail = new SimpleMailSender();
		try {
			mail.sendMessage(toAddr, fromAddr, personName, subject, message);
		} catch (Exception e) {
			e.printStackTrace();
		}*/

		// 登録完了→一覧画面を表示する
		RequestDispatcher rd = request.getRequestDispatcher("contact.jsp");
		rd.forward(request, response);
	}


	/**
	 * JSPで表示するメッセージを設定する。
	 *
	 * @param request
	 *            サーブレットリクエスト
	 * @param message
	 *            メッセージ文字列
	 */
	protected void setMessage(HttpServletRequest request, String message) {
		request.setAttribute("message", message);
	}

}
