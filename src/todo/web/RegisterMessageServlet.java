package todo.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.mail.internet.InternetAddress;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import todo.dao.TodoDAO;
import todo.util.SimpleMailSender;
import todo.vo.TodoValueObject;

/**
 * 登録処理を行う。
 */
@WebServlet("/registerMessage")
public class RegisterMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		
		// リクエストパラメータを受け取り、更新VOに格納する準備をする
		String title = new String (request.getParameter("title").getBytes("ISO-8859-1"));
		String message = new String (request.getParameter("message").getBytes("ISO-8859-1"));
		//　送信者（名前）
		String fromName = new String (request.getParameter("fromName").getBytes("ISO-8859-1"));
		// 受信者（メールアドレス）
		String reAddr = request.getParameter("reAddr");
		System.out.println("readdr:" + reAddr);
		// 送信者のIPアドレス
		String ipAddr = request.getParameter("ipAddr");
		int employeeID = Integer.parseInt(request.getParameter("employeeID"));
		String address = request.getParameter("address");
		
		List<InternetAddress> list = new ArrayList<InternetAddress>();
		String send[] = request.getParameterValues("send");
		if(send!=null && send.length == 0){
			for(int i = 0; send.length > i; i++){
				list.add(new InternetAddress(send[i], fromName));
			}
			SimpleMailSender mail = new SimpleMailSender();
			try {
				mail.sendMessage(list, fromName,address,"受信者", title, message);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// VOへ格納する。
		TodoValueObject vo = new TodoValueObject();
		vo.setSubject(title);
		vo.setMessage(message);
		vo.setContact(reAddr);
		vo.setSendIP(ipAddr);

		// DAOの取得
		TodoDAO dao = new TodoDAO();

		try {
			dao.insertMessage(vo);
			List<TodoValueObject> messageList = dao.getMessage();
			request.setAttribute("messageList", messageList);
			request.setAttribute("employeeID", employeeID);
		} catch (Exception e) {
			throw new ServletException(e);
		} finally {
			// DAOの処理が完了したら接続を閉じる
			dao.closeConnection();
		}

		// 登録完了→一覧画面を表示する
		RequestDispatcher rd = request.getRequestDispatcher("message.jsp");
		rd.forward(request, response);
	}


	/**
	 * JSPで表示するメッセージを設定する。
	 *
	 * @param request
	 *            サーブレットリクエスト
	 * @param message
	 *            メッセージ文字列
	 */
	protected void setMessage(HttpServletRequest request, String message) {
		request.setAttribute("message", message);
	}

}
