package todo.web;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.sql.Time;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import todo.dao.TodoDAO;
import todo.vo.TodoValueObject;

/**
 * 登録処理を行う。
 */
@WebServlet("/registerLink")
public class RegisterLinkServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		int move = 0;
		int num = 0;
		// リクエストパラメータを受け取り、更新VOに格納する準備をする
		
		String strMove = request.getParameter("move");
		String strNum = request.getParameter("num");
		
		if((strMove!=null&&strMove.length()!=0) && (strNum!=null&&strNum.length()!=0)){
			move =  Integer.parseInt(request.getParameter("move"));
			num = Integer.parseInt(request.getParameter("num"));
		}
		int linkID =  Integer.parseInt(request.getParameter("linkID"));
		String pageName = new String (request.getParameter("edit").getBytes("ISO-8859-1"));
		String linkURL = new String (request.getParameter("url").getBytes("ISO-8859-1"));
		int roomID =  Integer.parseInt(request.getParameter("roomID"));
		
		// VOへ格納する。
		TodoValueObject vo = new TodoValueObject();
		vo.setNum(num);
		vo.setLinkID(linkID);
		vo.setPageName(pageName);
		vo.setLinkURL(linkURL);
		vo.setRoomID(roomID);

		// 入力チェックを行う。
		//boolean checkResult = vo.valueCheck();

		// もし入力チェックエラーがあった場合は、エラーメッセージを表示し、再入力させるため元の詳細画面へ戻る
		/**if (! checkResult ) {
			request.setAttribute("errorMessages", vo.getErrorMessages());
			// タスク１件のvoをリクエスト属性へバインド
			request.setAttribute("vo", vo);

			// 詳細画面を表示する
			RequestDispatcher rd = request.getRequestDispatcher("/detail.jsp");
			rd.forward(request, response);
			return;
		}**/

		// DAOの取得
		TodoDAO dao = new TodoDAO();
		//String message = "";
		try {
			// 更新または登録処理を行う
			// 最終更新日時がnullのときは新規登録、それ以外は更新
			if (linkID == 0) {
				dao.insertLink(vo);
				//message = "タスクの新規登録処理が完了しました。";
				//setMessage(request, message);
			} else {
				dao.updateLink(vo, move);
				//message = "タスク[ " + id + " ]の更新処理が完了しました。";
				//setMessage(request, message);
			}
			List<TodoValueObject> linkList = dao.getLink(roomID);
			request.getSession().setAttribute("linkList", linkList);
		} catch (Exception e) {
			throw new ServletException(e);
		} finally {
			// DAOの処理が完了したら接続を閉じる
			dao.closeConnection();
		}

		// 登録完了→一覧画面を表示する
		RequestDispatcher rd = request.getRequestDispatcher("link.jsp");
		rd.forward(request, response);
	}


	/**
	 * JSPで表示するメッセージを設定する。
	 *
	 * @param request
	 *            サーブレットリクエスト
	 * @param message
	 *            メッセージ文字列
	 */
	protected void setMessage(HttpServletRequest request, String message) {
		request.setAttribute("message", message);
	}

}
