package todo.web;

import java.io.IOException;
import java.util.Date;
import java.sql.Time;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import todo.dao.TodoDAO;
import todo.vo.TodoValueObject;

/**
 * 登録処理を行う。
 */
@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータを受け取り、更新VOに格納する準備をする
		int employeeID = Integer.parseInt(request.getParameter("employeeID"));
		int statesID = Integer.parseInt(request.getParameter("statesID"));
		int roomID = Integer.parseInt(request.getParameter("roomSelect"));
		String firstName = new String (request.getParameter("firstName").getBytes("ISO-8859-1"));
		String lastName = new String (request.getParameter("lastName").getBytes("ISO-8859-1"));
		String firstKana = new String (request.getParameter("firstKana").getBytes("ISO-8859-1"));
		String lastKana = new String (request.getParameter("lastKana").getBytes("ISO-8859-1"));
		Boolean viewIP = Boolean.valueOf(request.getParameter("viewIP"));
		Boolean viewID = Boolean.valueOf(request.getParameter("viewID"));
		String ipAddress = request.getParameter("ipaddress");
		String localPhoneNumber = request.getParameter("address");
		String status = new String (request.getParameter("states").getBytes("ISO-8859-1"));
		String message = new String (request.getParameter("memo").getBytes("ISO-8859-1"));
		
		//現在時刻の取得
	    Date date1 = new Date();
	    Time time1 = new Time(date1.getTime());
	    
		// VOへ格納する。
		TodoValueObject vo = new TodoValueObject();
		vo.setEmployeeID(employeeID);
		vo.setStatusID(statesID);
		vo.setFirstName(firstName);
		vo.setLastName(lastName);
		vo.setFirstKana(firstKana);
		vo.setLastKana(lastKana);
		vo.setIpAddress(ipAddress);
		vo.setViewIP(viewIP);
		vo.setViewID(viewID);
		vo.setLocalPhoneNumber(localPhoneNumber);
		vo.setStatus(status);
		//vo.setLasuUpdate(lasuUpdate);
		vo.setRoomID(roomID);
		vo.setMessage(message);
		vo.setStartTime(time1);

		// 入力チェックを行う。
		//boolean checkResult = vo.valueCheck();

		// もし入力チェックエラーがあった場合は、エラーメッセージを表示し、再入力させるため元の詳細画面へ戻る
		/**if (! checkResult ) {
			request.setAttribute("errorMessages", vo.getErrorMessages());
			// タスク１件のvoをリクエスト属性へバインド
			request.setAttribute("vo", vo);

			// 詳細画面を表示する
			RequestDispatcher rd = request.getRequestDispatcher("/detail.jsp");
			rd.forward(request, response);
			return;
		}**/

		// DAOの取得
		TodoDAO dao = new TodoDAO();
		//String message = "";
		try {
			// 更新または登録処理を行う
			if (!(status.equals("null"))) {
				dao.registerInsert(vo);
				//message = "タスクの新規登録処理が完了しました。";
				//setMessage(request, message);
			} else {
				dao.updateEmployee(vo);
				//message = "タスク[ " + id + " ]の更新処理が完了しました。";
				//setMessage(request, message);
			}
		} catch (Exception e) {
			throw new ServletException(e);
		} finally {
			// DAOの処理が完了したら接続を閉じる
			dao.closeConnection();
		}

		/*
		String toAddr = "toaddr@example.com";
		String fromAddr = "fromaddr@example.com";
		String personName = "Mail Test User";
		String subject = "TODO管理アプリケーションからの報告です";
		// 完了時にメールを送信する
		SimpleMailSender mail = new SimpleMailSender();
		try {
			mail.sendMessage(toAddr, fromAddr, personName, subject, message);
		} catch (Exception e) {
			e.printStackTrace();
		}*/

		// 登録完了→一覧画面を表示する
		RequestDispatcher rd = request.getRequestDispatcher("/search?pull=" + roomID);
		rd.forward(request, response);
	}


	/**
	 * JSPで表示するメッセージを設定する。
	 *
	 * @param request
	 *            サーブレットリクエスト
	 * @param message
	 *            メッセージ文字列
	 */
	protected void setMessage(HttpServletRequest request, String message) {
		request.setAttribute("message", message);
	}

}
