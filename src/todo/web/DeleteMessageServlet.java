package todo.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import todo.dao.TodoDAO;
import todo.vo.TodoValueObject;

/**
 * 削除処理を行う。
 */
@WebServlet("/deleteMessage")
public class DeleteMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// DAOの取得
		TodoDAO dao = new TodoDAO();

		// リクエストパラメータから選択したタスクidを取得する
		String paramId = request.getParameter("id");
		String paramId2 = request.getParameter("e_id");

		try {
			// intへ変換※NumberFormatExceptionが発生する可能性あり
			int id = Integer.parseInt(paramId);
			int e_id = Integer.parseInt(paramId2);

			// Stringからintへ変換し、daoで処理を行う。対象のタスクを１件削除し、成功すると１が返される。
			int result = dao.deleteMessage(id);
			
			List<TodoValueObject> messageList = dao.getMessage();
			request.setAttribute("messageList", messageList);
			request.setAttribute("employeeID", e_id);
			
			
		} catch (Exception e) {
			throw new ServletException(e);
		} finally {
			// DAOの処理が完了したら接続を閉じる
			dao.closeConnection();
		}

		//setMessage(request, "タスク[ " + paramId + " ]の削除処理が完了しました。");

		// 画面を返す
		// 完了画面を表示する
		RequestDispatcher rd = request.getRequestDispatcher("message.jsp");
		rd.forward(request, response);
	}

	/**
	 * JSPで表示するメッセージを設定する。
	 *
	 * @param request
	 *            サーブレットリクエスト
	 * @param message
	 *            メッセージ文字列
	 */
	protected void setMessage(HttpServletRequest request, String message) {
		request.setAttribute("message", message);
	}
}
