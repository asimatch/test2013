package todo.web;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import todo.dao.TodoDAO;
import todo.vo.TodoValueObject;

/**
 * 登録処理を行う。
 */
@WebServlet("/statesUpdate")
public class StatesUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
				
		// リクエストパラメータを受け取り、更新VOに格納する準備をする
		int statesID = Integer.parseInt(request.getParameter("states"));		
		int empID = Integer.parseInt(request.getParameter("empID"));
		int roomID = Integer.parseInt(request.getParameter("roomID"));
		
		// VOへ格納する。
		TodoValueObject vo = new TodoValueObject();
		vo.setStatusID(statesID);
		vo.setEmployeeID(empID);

		// DAOの取得
		TodoDAO dao = new TodoDAO();

		try {
			dao.updateEmpStates(vo);
		} catch (Exception e) {
			throw new ServletException(e);
		} finally {
			// DAOの処理が完了したら接続を閉じる
			dao.closeConnection();
		}


		// 登録完了→一覧画面を表示する
		RequestDispatcher rd = request.getRequestDispatcher("/search?pull=" + roomID);
		rd.forward(request, response);
	}
}
