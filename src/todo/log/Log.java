package todo.log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

import todo.dao.TodoDAO;

public class Log {

	//年・月・日・時間・分・秒の変数（数値型）
	int y, mo, d, h, mi, s;

	//年・月・日・時間・分・秒の変数（文字型）
	String year, month, day, hour, minute, second;

	//﻿○○年○月○日○時○分○秒
	String ymdhms;


	//コンストラクタ
	Log(){

		//オブジェクトの生成
		Calendar cal1 = Calendar.getInstance();


		//現在の年を取得
		y = cal1.get(Calendar.YEAR);

		//現在の月を取得
		mo = cal1.get(Calendar.MONTH) + 1;

		//現在の日を取得
		d = cal1.get(Calendar.DATE);

		//現在の時を取得
		h = cal1.get(Calendar.HOUR_OF_DAY);

		//現在の分を取得
		mi = cal1.get(Calendar.MINUTE);

		//現在の秒を取得
		s = cal1.get(Calendar.SECOND);


		//数値型を文字型へ変換
		year = String.valueOf(y);
		month = String.valueOf(mo);
		day = String.valueOf(d);
		hour = String.valueOf(h);
		minute = String.valueOf(mi);
		second = String.valueOf(s);

		//一つの変数に格納
		ymdhms = year + "年" + month + "月" + day + "日" + hour + "時"+ minute + "分" + second + "秒";
	}

	//追加ログ
	void insertLog(TodoDAO td){

		try{
			File file = new File("test.txt");

			FileWriter filewriter = new FileWriter(file, true);

			if (file.exists()){

			}else{
				file.createNewFile();
			}

			//追加したレコードの詳細をログに書き込む
			for(int i = 0; i <= td.getClumName_().length; i++){

				if(i == 0){
					filewriter.write(ymdhms + "   " + td.getTableName_() + "テーブルに " + td.getClumName_()[i] + ":" + td.getRecord_()[i] + " ");

				}else if(i == td.getClumName_().length){
					filewriter.write("を追加しました。\r\n");

				}else{
					filewriter.write(td.getClumName_()[i] + ":" + td.getRecord_()[i] + " ");
				}
			}

			filewriter.close();

		}catch(IOException e){
			System.out.println(e);
		}
	}

	//更新ログ
	void updateLog(TodoDAO td){

		//変更箇所があるかどうかをチェックする変数
		boolean check = false;

		try{
			File file = new File("test.txt");

			FileWriter filewriter = new FileWriter(file, true);

			if (file.exists()){

			}else{
				file.createNewFile();
			}

			//更新されたレコードの詳細をログに書き込む
			for(int i = 0; i <= td.getClumName_().length; i++){

				if(i == td.getClumName_().length){

					//更新箇所があった場合
					if(check){
						filewriter.write("更新しました。\r\n");
					}

					//更新箇所が何もなかった場合
					else{
						filewriter.write("何も更新されていません。\r\n");
					}

				}else if(i == 0){
					filewriter.write(ymdhms + "   " + td.getTableName_() + "テーブルの ");

					//キーが複数ある場合を考慮してのfor文
					for(int j = 0; j < td.getKey_().length; j++){
						if(j > 0){
							filewriter.write(", " + td.getKey_()[j]);
						}else{
							filewriter.write("キー：" + td.getKey_()[j]);
						}
					}

					filewriter.write(" のレコードにて ");
				}

				//更新箇所があった場合はログに詳細を書き込む
				else if(td.getBefore_()[i] != td.getAfter_()[i]){
					filewriter.write(" " + td.getClumName_()[i] + ":" +  td.getBefore_()[i] + " を 「" + td.getAfter_()[i] + "」 に");

					//チェック変数をtrueへ
					check = true;
				}
			}

			filewriter.close();

		}catch(IOException e){
			System.out.println(e);
		}
	}

	//削除ログ
	void deletelog(TodoDAO td){

		try{
			File file = new File("test.txt");

			FileWriter filewriter = new FileWriter(file, true);

			if (file.exists()){

			}else{
				file.createNewFile();
			}

			//削除したレコードの詳細をログに書き込む
			for(int i = 0; i <= td.getClumName_().length; i++){

				if(i == td.getClumName_().length){
					filewriter.write("のレコードを削除しました。\r\n");

				}else if(i == 0){
					filewriter.write(ymdhms + "   " + td.getTableName_() + "テーブルの " + td.getClumName_()[i] + ":" + td.getRecord_()[i] + " ");

				}else{
					filewriter.write(td.getClumName_()[i] + ":" + td.getRecord_()[i] + " ");
				}
			}

			filewriter.close();

		}catch(IOException e){
			System.out.println(e);
		}
	}

	void input(){

		try{
			File file = new File("test.txt");


			BufferedReader br = new BufferedReader(new FileReader(file));

			String str;
			while((str = br.readLine()) != null){
				System.out.println(str);
			}

			br.close();

		}catch(IOException e){
			System.out.println(e);
		}
	}
}
