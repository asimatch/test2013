package todo.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.sun.org.apache.bcel.internal.generic.RET;

import todo.log.Log;
import todo.vo.TodoValueObject;

public class TodoDAO extends CommonMySQLDAO {


	//ログの引数：テーブル名
	public String tableName_;

	//ログの引数：カラム
	public String[] clumName_;

	//ログの引数：登録するレコード、または削除するレコードを格納する配列
	public String[] record_;

	//ログの引数：更新前レコードを格納する配列
	public String[] before_;

	//ログの引数：更新後レコードを格納する配列
	public String[] after_;

	//ログの引数：キーを格納する配列
	public String[] key_;

	public List<TodoValueObject> todoList(int roomID) throws Exception {
		List<TodoValueObject> returnList = new ArrayList<TodoValueObject>();

		// その部屋に登録されている人の名前や座標など執務室レイアウト表示に必要な情報を取得するsql
		String sql = "SELECT EmployeeID, FirstName, LastName, FirstKana, LastKana, ViewIP, ViewID, TableX, TableY, Width, Height, Color, IPAddress, LocalPhoneNumber FROM " +
				"seating.employee emp LEFT OUTER JOIN seating.status sta ON emp.StatusID = sta.StatusID " +
				"WHERE RoomID = ? ORDER BY TableX, TableY";

		// プリペアステートメントを取得し、実行SQLを渡す
		PreparedStatement statement = getPreparedStatement(sql);

		statement.setInt(1, roomID);

		// SQLを実行してその結果を取得する。
		ResultSet rs = statement.executeQuery();


		// 検索結果の行数分フェッチを行い、取得結果をValueObjectへ格納する
		while (rs.next()) {
			TodoValueObject vo = new TodoValueObject();

			vo.setEmployeeID(rs.getInt("EmployeeID"));
			vo.setFirstName(rs.getString("FirstName"));
			vo.setLastName(rs.getString("LastName"));
			vo.setFirstKana(rs.getString("FirstKana"));
			vo.setLastKana(rs.getString("LastKana"));
			vo.setViewIP(rs.getBoolean("ViewIP"));
			vo.setViewID(rs.getBoolean("ViewID"));
			vo.setWidth(rs.getInt("Width"));
			vo.setHeight(rs.getInt("Height"));
			vo.setTableX(rs.getInt("TableX"));
			vo.setTableY(rs.getInt("TableY"));
			vo.setColor(rs.getString("Color"));
			vo.setIpAddress(rs.getString("IPAddress"));
			vo.setLocalPhoneNumber(rs.getString("LocalPhoneNumber"));
			
			vo.setRoomID(roomID);

			returnList.add(vo);
		}

		return returnList;
	}

	/** -------登録メソッド一覧-------　**/

	/**
	 * 社員登録を行う。
	 * @param vo 入力されたタスク内容。
	 * @return 追加された件数
	 * @throws Exception
	 */
	public int registerInsert(TodoValueObject vo) throws Exception {

		/**String sql = "INSERT INTO seating.employee (EmployeeID,FirstName,LastName,IPAddress,ViewIP," +
				"ViewID,LocalPhoneNumber,StatusID,LastUpdate,RoomID,Message,LeftX,LeftY,RightX,RightY,destination," +
				"startTime,endTime,neFLG,nrFLG)"  +
				"VALUES (?,?,?,?,?,?,?,?,now(),?,?,null,null,null,null,null,null,null,false,false)";*/
		String sql = "INSERT INTO seating.employee (EmployeeID,FirstName,LastName,FirstKana,LastKana,IPAddress," +
				"LocalPhoneNumber,StatusID,Message) " +
				"VALUES (?,?,?,?,?,?,?,?,?)";

		int result = 0;
		// プリペアステートメントを取得し、実行SQLを渡す
		try {
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setInt(1, vo.getEmployeeID());
			statement.setString(2, vo.getFirstName());
			statement.setString(3, vo.getLastName());
			statement.setString(4, vo.getFirstKana());
			statement.setString(5, vo.getLastKana());
			statement.setString(6, vo.getIpAddress());
			statement.setString(7, vo.getLocalPhoneNumber());
			statement.setInt(8, vo.getStatusID());
			statement.setString(9, vo.getMessage());

			/**statement.setInt(11, vo.getLeftX());
			statement.setInt(12, vo.getLeftY());
			statement.setInt(13, vo.getRightX());
			statement.setInt(14, vo.getRightY());
			statement.setString(15, vo.getDestination());
			statement.setTime(16, vo.getStartTime());
			statement.setTime(17, vo.getEndTime());
			statement.setBoolean(18, vo.isNeFLG());
			statement.setBoolean(19, vo.isNrFLG());*/


			//Time型をString型へキャストする
			//String S = new SimpleDateFormat("hh:mm:ss").format(vo.getStartTime());
			//String E = new SimpleDateFormat("hh:mm:ss").format(vo.getEndTime());

			//ログの引数：登録するレコード
			/**record_ = new String[]{String.valueOf(vo.getEmployeeID()), vo.getFirstName(), vo.getLastName(), vo.getIpAddress(), String.valueOf(vo.isViewIP()),
					String.valueOf(vo.isViewID()), String.valueOf(vo.getLocalPhoneNumber()), String.valueOf(vo.getStatusID()), String.valueOf(vo.getRoomID()),
					vo.getMessage(), String.valueOf(vo.getLeftX()), String.valueOf(vo.getLeftY()), String.valueOf(vo.getRightX()), String.valueOf(vo.getRightY()),
					vo.getDestination(), S, E, String.valueOf(vo.isNeFLG()), String.valueOf(vo.isNrFLG())};*/



			result = statement.executeUpdate();

			// コミットを行う
			super.commit();
		} catch (Exception e) {
			// ロールバックを行い、スローした例外はDAOから脱出する
			super.rollback();
			throw e;
		}

		return result;
	}

	/**
	 * ●部屋登録処理を行う。
	 * @param vo 入力されたタスク内容。
	 * @return 追加された件数
	 * @throws Exception
	 */
	public int insertRoom(TodoValueObject vo) throws Exception {

		//ログの引数：テーブル名
				tableName_ = "Room";

				//ログの引数：カラム
				clumName_ = new String[]{"RoomName, Width, Height"};


		String sql = "INSERT INTO seating.room (RoomName, Width, Height, DepartmentID) " +
				"VALUES (?,?,?,?)";
		
		String sql2 = "INSERT INTO seating.roomanddepartment (RoomID, DepartmentID) " +
				"SELECT MAX(RoomID), ? FROM seating.room";

		int result = 0;
		// プリペアステートメントを取得し、実行SQLを渡す
		try {
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setString(1, vo.getRoomName());
			statement.setInt(2, vo.getWidth());
			statement.setInt(3, vo.getHeight());
			statement.setInt(4, vo.getDepartmentID());
			
			PreparedStatement statement2 = getPreparedStatement(sql2);
			statement2.setInt(1, vo.getDepartmentID());

			//Time型をString型へキャストする
			//String S = new SimpleDateFormat("hh:mm:ss").format(vo.getStartTime());
		//	String E = new SimpleDateFormat("hh:mm:ss").format(vo.getEndTime());

			//ログの引数：登録するレコード
			record_ = new String[]{String.valueOf(vo.getRoomID()), vo.getRoomName(), String.valueOf(vo.getWidth()), String.valueOf(vo.getIpAddress())};

			result = statement.executeUpdate();
			statement2.executeUpdate();
			// コミットを行う
			super.commit();
		} catch (Exception e) {
			// ロールバックを行い、スローした例外はDAOから脱出する
			super.rollback();
			throw e;
		}

		return result;
	}

	/**
	 * ●社内共有携帯電話の追加。
	 * @param vo 入力されたタスク内容。
	 * @return 追加された件数
	 * @throws Exception
	 */
	public int insertPhone(TodoValueObject vo) throws Exception {

		//ログの引数：テーブル名
		tableName_ = "RentalPhone";

		//ログの引数：カラム
		clumName_ = new String[]{"TelNo, Type, PhoneStatus, Memo"};

		String sql = "INSERT INTO seating.RentalPhone (TelNo, PhoneStatus, Memo)" +
				"VALUES (?,?,?)";

		int result = 0;
		// プリペアステートメントを取得し、実行SQLを渡す
		try {
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setString(1, vo.getTelNo());
			statement.setString(2, vo.getPhoneStatus());
			statement.setString(3, vo.getMemo());

			//Time型をString型へキャストする
			//String S = new SimpleDateFormat("hh:mm:ss").format(vo.getStartTime());
			//String E = new SimpleDateFormat("hh:mm:ss").format(vo.getEndTime());

			//ログの引数：登録するレコード
			record_ = new String[]{String.valueOf(vo.getTelNo()), String.valueOf(vo.getType()), String.valueOf(vo.getPhoneStatus()), String.valueOf(vo.getMemo())};


			result = statement.executeUpdate();

			// コミットを行う
			super.commit();
		} catch (Exception e) {
			// ロールバックを行い、スローした例外はDAOから脱出する
			super.rollback();
			throw e;
		}

		return result;
	}

	/**
	 * ●ステータス凡例の追加。
	 * @param vo 入力されたタスク内容。
	 * @return 追加された件数
	 * @throws Exception
	 */
	public int insertStatus(TodoValueObject vo) throws Exception {

		//ログの引数：テーブル名
		tableName_ = "Status";

		//ログの引数：カラム
		clumName_ = new String[]{"StatusID, Status, Color"};

		String sql = "INSERT INTO seating.status (StatusID, Status, Color) " +
				"VALUES (?,?,?)";

		int result = 0;
		// プリペアステートメントを取得し、実行SQLを渡す
		try {
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setInt(1, vo.getStatusID());
			statement.setString(2, vo.getStatus());
			statement.setString(3, vo.getColor());

			//Time型をString型へキャストする
			//String S = new SimpleDateFormat("hh:mm:ss").format(vo.getStartTime());
			//String E = new SimpleDateFormat("hh:mm:ss").format(vo.getEndTime());

			//ログの引数：登録するレコード
			record_ = new String[]{String.valueOf(vo.getPhoneID()), vo.getStatus(), vo.getColor()};


			result = statement.executeUpdate();

			// コミットを行う
			super.commit();
		} catch (Exception e) {
			// ロールバックを行い、スローした例外はDAOから脱出する
			super.rollback();
			throw e;
		}

		return result;
	}



	/**
	 * ●伝言登録処理を行う。
	 * @param vo 入力された伝言内容。
	 * @return 追加された件数
	 * @throws Exception
	 */
	public int insertMessage(TodoValueObject vo) throws Exception {

		//ログの引数：テーブル名
				tableName_ = "Message";

				//ログの引数：カラム
			//	clumName_ = new String[]{"MessageID","SendIP","ReceiveID","Message","Time","BookMark","ReadFLG"};


							
		String sql = "INSERT INTO seating.Message (ReceiveID,SendIP,Message,Subject) " +
				"VALUES ( ( SELECT EmployeeID FROM seating.EmployeeDetail WHERE Contact = ?), ?, ?, ? )";
		int result = 0;
		// プリペアステートメントを取得し、実行SQLを渡す
		try {
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setString(1, vo.getContact());
			statement.setString(2, vo.getSendIP());
			statement.setString(3, vo.getMessage());
			statement.setString(4, vo.getSubject());

			result = statement.executeUpdate();

			//Time型をString型へキャストする
			//String T = new SimpleDateFormat("hh:mm:ss").format(vo.getTime());

			//ログの引数：登録するレコード
			//record_ = new String[]{String.valueOf(vo.getMessageID()), String.valueOf(vo.getSendIP()), String.valueOf(vo.getReceiveID()), vo.getMessage(),
					//T, String.valueOf(vo.isBookMark()), String.valueOf(vo.isReadFLG())};


			// コミットを行う
			super.commit();
		} catch (Exception e) {
			// ロールバックを行い、スローした例外はDAOから脱出する
			super.rollback();
			throw e;
		}

		return result;
	}

	/**
	 * ●連絡先登録処理を行う。
	 * メール機能の連絡先
	 * @param vo 入力されたタスク内容。
	 * @return 追加された件数
	 * @throws Exception
	 */
	public int insertSend(TodoValueObject vo) throws Exception {

		//ログの引数：テーブル名
				tableName_ = "EmployeeDetail";

				//ログの引数：カラム
				clumName_ = new String[]{"EmployeeID","Contact","Detail"};

		String sql = "INSERT INTO seating.EmployeeDetail (EmployeeID,Contact,Detail " +
				"VALUES (?,?,?)";

		int result = 0;
		// プリペアステートメントを取得し、実行SQLを渡す
		try {
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setInt(1, vo.getEmployeeID());
			statement.setString(2, vo.getFirstName());
			statement.setString(3, vo.getLastName());

			result = statement.executeUpdate();

			//ログの引数：登録するレコード
			record_ = new String[]{String.valueOf(vo.getEmployeeID()), vo.getContact(), vo.getDetail()};


			// コミットを行う
			super.commit();
		} catch (Exception e) {
			// ロールバックを行い、スローした例外はDAOから脱出する
			super.rollback();
			throw e;
		}

		return result;
	}

	/**
	 * ●部署登録処理を行う。
	 * @param vo 入力されたタスク内容。
	 * @return 追加された件数
	 * @throws Exception
	 */
	public int insertDepartment(TodoValueObject vo) throws Exception {

		String sql = "INSERT INTO seating.Department (DepartmentName) VALUES (?)";

		int result = 0;
		// プリペアステートメントを取得し、実行SQLを渡す
		try {
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setString(1, vo.getDepartmentName());

			result = statement.executeUpdate();

			// コミットを行う
			super.commit();
		} catch (Exception e) {
			// ロールバックを行い、スローした例外はDAOから脱出する
			super.rollback();
			throw e;
		}

		return result;
	}
	
	/**
	 * ●リンク登録処理を行う。
	 * @param vo 入力されたタスク内容。
	 * @return 追加された件数
	 * @throws Exception
	 */
	public int insertLink(TodoValueObject vo) throws Exception {

		String sql = "INSERT INTO seating.link (Num, PageName, LinkURL, RoomID) " +
				"SELECT MAX(Num) + 1,?,?,? FROM seating.link";
		
		int result = 0;
		// プリペアステートメントを取得し、実行SQLを渡す
		try {
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setString(1, vo.getPageName());
			statement.setString(2, vo.getLinkURL());
			statement.setInt(3, vo.getRoomID());

			result = statement.executeUpdate();
			// コミットを行う
			super.commit();
		} catch (Exception e) {
			// ロールバックを行い、スローした例外はDAOから脱出する
			super.rollback();
			throw e;
		}

		return result;
	}
	
	/** -------更新メソッド一覧-------　**/


	/**
	 * ●部屋更新処理を行う。
	 * @param
	 * @return
	 * @throws Exception
	 */
	public int updateRoom(TodoValueObject vo) throws Exception {
		int result =0;

			String sql = "UPDATE seating.room SET RoomName = ? ,DepartmentID = ? WHERE RoomID = ?";

			try{
			// プリペアステートメントを取得し、実行SQLを渡す
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setString(1, vo.getRoomName());
			statement.setInt(2, vo.getDepartmentID());
			statement.setInt(3, vo.getRoomID());
			result = statement.executeUpdate();

				// コミットを行う
				super.commit();

			} catch (Exception e) {
				super.rollback();
				throw e;
			}
		

		return result;
	}


	/**
	 * ●ステータス更新を行う。
	 * @param
	 * @return
	 * @throws Exception
	 */
	public int updateEmpStates(TodoValueObject vo) throws Exception {
		int result =0;
			String sql = "UPDATE seating.employee SET StatusID = ? WHERE EmployeeID = ?";
			try{
				// プリペアステートメントを取得し、実行SQLを渡す
				PreparedStatement statement = getPreparedStatement(sql);
				statement.setInt(1, vo.getStatusID());
				statement.setInt(2, vo.getEmployeeID());
				result = statement.executeUpdate();
				// コミットを行う
				super.commit();
			} catch (Exception e) {
				super.rollback();
				throw e;
			}
		return result;
	}
	

	/**
	 * ステータスの更新処理を行う。
	 * @param statusID,names
	 * @return
	 * @throws Exception
	 */
	public int statusUpdate(String statusID , String names) throws Exception {
		int result =0;
		String nameAry[] = names.split(",");

		//ログの引数：テーブル名
		tableName_ = "status";

		//ログの引数：キーを格納する配列
		key_ = new String[nameAry.length];

		//ログの引数：カラム
		clumName_ = new String[]{"statusID", "status", "color"};

		//ログの引数：更新前レコードを格納する配列
		before_ = new String[clumName_.length];

		//ログの引数：更新後レコードを格納する配列
		after_ = new String[clumName_.length];


		for(int i = 0;i < nameAry.length;i++){
			String sql = "UPDATE seating.status SET statusID = ? WHERE firstName = ?";


			//更新前のレコードを取り出すSQL文
			String para = "SELECT * FROM seating.status WHERE firstName = ?";

			// プリペアステートメントを取得し、実行SQLを渡す
			try {


				//引数:更新前レコードをそれぞれ配列に格納する
				PreparedStatement st = getPreparedStatement(para);
				st.setString(1, nameAry[i]);
				ResultSet rs = st.executeQuery();
				while(rs.next()) {

					before_[0] = rs.getString("statusID");
					before_[1] = rs.getString("status");
					before_[2] = rs.getString("color");

					//ログの引数:レコードをそれぞれ配列に格納する
					after_[1] = rs.getString("status");
					after_[2] = rs.getString("color");
				}

				//ログの引数：キーを配列に格納する
				key_[i] = nameAry[i];


				PreparedStatement statement = getPreparedStatement(sql);
				statement.setString(1, statusID);
				statement.setString(2, nameAry[i]);
				result = statement.executeUpdate();

				// コミットを行う
				super.commit();


				//ログの引数:更新後レコードを配列に格納する
				after_[0] = statusID;



			} catch (Exception e) {
				super.rollback();
				throw e;
			}
		}
		return result;
	}

	/**
	 * 社員のステータスの更新処理を行う。
	 * @param statusID,names
	 * @return
	 * @throws Exception
	 */
	public int updateEmployeeStates(int statusID , String empIDs) throws Exception {
		int result =0;
		String idAry[] = empIDs.split(",");

		//ログの引数：テーブル名
		/*tableName_ = "status";

		//ログの引数：キーを格納する配列
		key_ = new String[nameAry.length];

		//ログの引数：カラム
		clumName_ = new String[]{"statusID", "status", "color"};

		//ログの引数：更新前レコードを格納する配列
		before_ = new String[clumName_.length];

		//ログの引数：更新後レコードを格納する配列
		after_ = new String[clumName_.length];*/


		for(int i = 0;i < idAry.length;i++){
			String sql = "UPDATE seating.Employee SET StatusID = ? WHERE EmployeeID = ?";


			//更新前のレコードを取り出すSQL文
			//String para = "SELECT * FROM seating.status WHERE firstName = ?";

			// プリペアステートメントを取得し、実行SQLを渡す
			try {


				//引数:更新前レコードをそれぞれ配列に格納する
				/*PreparedStatement st = getPreparedStatement(para);
				st.setString(1, nameAry[i]);
				ResultSet rs = st.executeQuery();
				while(rs.next()) {

					before_[0] = rs.getString("statusID");
					before_[1] = rs.getString("status");
					before_[2] = rs.getString("color");

					//ログの引数:レコードをそれぞれ配列に格納する
					after_[1] = rs.getString("status");
					after_[2] = rs.getString("color");
				}

				//ログの引数：キーを配列に格納する
				key_[i] = nameAry[i];*/


				PreparedStatement statement = getPreparedStatement(sql);
				statement.setInt(1, statusID);
				statement.setString(2, idAry[i]);
				result = statement.executeUpdate();

				// コミットを行う
				super.commit();


				//ログの引数:更新後レコードを配列に格納する
				//after_[0] = statusID;



			} catch (Exception e) {
				super.rollback();
				throw e;
			}
		}
		return result;
	}


	/**
	 * ステータスの更新処理を行う。
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	public int updateStates(TodoValueObject vo, int id) throws Exception {
		int result =0;

		//ログの引数：テーブル名
		tableName_ = "status";

		//ログの引数：カラム
		clumName_ = new String[]{"statusID", "status", "color"};

		//ログの引数：更新前レコードを格納する配列
		before_ = new String[clumName_.length];

		//ログの引数：更新後レコードを格納する配列
		after_ = new String[clumName_.length];


		String sql = "UPDATE seating.status SET StatusID = ?,Status = ?,Color = ? WHERE StatusID = ?";

		//更新前のレコードを取り出すSQL文
		String para = "SELECT * FROM seating.status WHERE Status = ?";
		// プリペアステートメントを取得し、実行SQLを渡す
		try {

			//引数:更新前レコードをそれぞれ配列に格納する
			PreparedStatement st = getPreparedStatement(para);
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			while(rs.next()) {

				before_[0] = String.valueOf(rs.getInt("statusID"));
				before_[1] = rs.getString("status");
				before_[2] = rs.getString("color");

				//ログの引数:レコードをそれぞれ配列に格納する
				after_[1] = rs.getString("status");
				after_[2] = rs.getString("color");
			}

			


			PreparedStatement statement = getPreparedStatement(sql);
			statement.setInt(1, vo.getStatusID());
			statement.setString(2, vo.getStatus());
			statement.setString(3, vo.getColor());
			statement.setInt(4, id);
			result = statement.executeUpdate();
			// コミットを行う
			super.commit();

			//ログの引数:更新後レコードを配列に格納する
			after_[0] = String.valueOf(vo.getStatusID());



			} catch (Exception e) {
				super.rollback();
				throw e;
			}
		return result;
	}
	
	
	

	/**
	 *　掲示板の更新処理を行う。
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	public int updateBulletin(int bulletinID, String bulletin) throws Exception {

		//ログの引数：テーブル名
		tableName_ = "Bulletin";

		//ログの引数：キー
		key_ = new String[]{bulletin};

		//ログの引数：カラム
		clumName_ = new String[]{"BulletinID", "Bulletin", "RoomID", "Ipaddress", "Update"};

		//ログの引数：更新前レコードを格納する配列
		before_ = new String[clumName_.length];

		//ログの引数：更新後レコードを格納する配列
		after_ = new String[clumName_.length];

		//更新前のレコードを取り出すSQL文
		String para = "SELECT * FROM seating.bulletin WHERE BulletinID = ?";


		int result =0;
		String sql = "UPDATE seating.bulletin SET Bulletin = ? WHERE BulletinID = ?";

		// プリペアステートメントを取得し、実行SQLを渡す
		try {

			//ログの引数：更新前レコードをそれぞれ格納する
			PreparedStatement st = getPreparedStatement(para);
			st.setInt(1, bulletinID);
			ResultSet rs = st.executeQuery();
			while(rs.next()) {

				before_[0] = rs.getString("BulletinID");
				before_[1] = rs.getString("Bulletin");
				before_[2] = rs.getString("RoomID");
				before_[3] = rs.getString("Ipaddress");
				before_[4] = rs.getString("Update");

				//ログの引数：レコードを格納する
				after_[0] = rs.getString("BulletinID");
				after_[2] = rs.getString("RoomID");
				after_[3] = rs.getString("Ipaddress");
				after_[4] = rs.getString("Update");
			}


			PreparedStatement statement = getPreparedStatement(sql);
			statement.setString(1, bulletin);
			statement.setInt(2, bulletinID);
			result = statement.executeUpdate();

			// コミットを行う
			super.commit();


			//ログの引数：更新後レコードを格納する
			after_[1] = bulletin;

		} catch (Exception e) {
			super.rollback();
			throw e;
		}
		return result;
	}
	
	/**
	 *　リンクの更新処理を行う。
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	public int updateLink(TodoValueObject vo, int upDown) throws Exception {

		int result =0;
		int linkID = 0;
				
		String sql = "UPDATE seating.link SET PageName = ?, LinkURL = ? WHERE LinkID = ?";
				
		String sql2 = "SELECT LinkID FROM seating.link WHERE RoomID = ? AND Num = ?";
		
		String sql3 = "UPDATE seating.link SET Num = Num + ? WHERE LinkID = ?";
		
		String sql4 = "UPDATE seating.link SET Num = Num - ? WHERE LinkID = ?";
				
		try {

			PreparedStatement statement = getPreparedStatement(sql2);
			statement.setInt(1, vo.getRoomID());
			statement.setInt(2, vo.getNum() + upDown);
			ResultSet rs = statement.executeQuery();
			rs.next();
			linkID = rs.getInt("LinkID");
			// コミットを行う
			super.commit();

		} catch (Exception e) {
			super.rollback();
			throw e;
		}
		
		try {

			PreparedStatement statement = getPreparedStatement(sql);
			statement.setString(1, vo.getPageName());
			statement.setString(2, vo.getLinkURL());
			statement.setInt(3, vo.getLinkID());
			result = statement.executeUpdate();
			
			PreparedStatement statement2 = getPreparedStatement(sql3);
			statement2.setInt(1, upDown);
			statement2.setInt(2, vo.getLinkID());
			result = statement2.executeUpdate();
			
			PreparedStatement statement3 = getPreparedStatement(sql4);
			statement3.setInt(1, upDown);
			statement3.setInt(2, linkID);
			result = statement3.executeUpdate();

			// コミットを行う
			super.commit();

		} catch (Exception e) {
			super.rollback();
			throw e;
		}
		return result;
	}
	
	/**
	 * 伝言の更新処理を行う。
	 * @param messageID
	 * @return
	 * @throws Exception
	 */
	public int updateMessage(int messageID, boolean flag) throws Exception {
		int result =0;
		String sql = "UPDATE seating.message SET BookMark = ? WHERE MessageID = ?";
		// プリペアステートメントを取得し、実行SQLを渡す
		try {
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setBoolean(1, flag);
			statement.setInt(2, messageID);
			result = statement.executeUpdate();
			// コミットを行う
			super.commit();
		} catch (Exception e) {
			super.rollback();
			throw e;
		}
		return result;
	}
	
	/**
	 * 社員情報の更新処理を行う。
	 * @return
	 * @throws Exception
	 */
	public int updateEmployee(TodoValueObject vo) throws Exception {
		int result =0;
		String sql = "UPDATE seating.employee SET FirstName = ?,LastName = ?,FirstKana = ?,LastKana = ?,IPAddress = ?, " +
				"ViewIP = ?,ViewID = ?,LocalPhoneNumber = ?,RoomID = ? WHERE EmployeeID = ?";
		// プリペアステートメントを取得し、実行SQLを渡す
		try {
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setString(1, vo.getFirstName());
			statement.setString(2, vo.getLastName());
			statement.setString(3, vo.getFirstKana());
			statement.setString(4, vo.getLastKana());
			statement.setString(5, vo.getIpAddress());
			statement.setBoolean(6, vo.isViewIP());
			statement.setBoolean(7, vo.isViewID());
			statement.setString(8, vo.getLocalPhoneNumber());
			statement.setInt(9, vo.getRoomID());
			statement.setInt(10, vo.getEmployeeID());
			result = statement.executeUpdate();
			// コミットを行う
			super.commit();
		} catch (Exception e) {
			super.rollback();
			throw e;
		}
		return result;
	}
	
	/**
	 * ●既読フラグの更新
	 * @param MessageID
	 * @return
	 * @throws Exception
	 */
	public int checkMessage(int employeeID) throws Exception {
		int result = 0;
		
		//伝言を取得したのが受信者だったら既読フラグを立てる
		String sql = "UPDATE seating.Message SET ReadFLG = 1 WHERE ReceiveID = ?";

		// プリペアステートメントを取得し、実行SQLを渡す
		try {
			// プリペアステートメントを取得し、実行SQLを渡す
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setInt(1, employeeID);
			result = statement.executeUpdate();
			// コミットを行う
			super.commit();
		} catch (Exception e) {
			super.rollback();
			throw e;
		}
		return result;
	}
	
	/** -------削除メソッド一覧-------　**/

	/**
	 * ●個人詳細情報削除処理を行う。指定されたEmployeeIDのタスクを削除する。
	 * @param employeeID
	 * @return 削除件数
	 * @throws Exception
	 */
	public int deleteDetail(int employeeID) throws Exception {

		//ログの引数：テーブル名
		tableName_ = "Employee";

		//ログの引数：カラム
		clumName_ = new String[]{"EmployeeID","FirstName","LastName","IPAddress","ViewIP","ViewID","LocalPhoneNumber","StatusID","LastUpdate",
				"RoomID","Message","LeftX","LeftY","RightX","RightY","destination","startTime","endTime","neFLG","nrFLG"};

		//ログの引数：削除するレコードを格納する配列
		record_ = new String[clumName_.length];

		//削除するレコードを取り出す取り出すSQL文
		String para = "SELECT * FROM seating.employee WHERE EmployeeID = ?";


		String sql = "DELETE FROM seating.employee where EmployeeID = ?";



		// SQLを実行してその結果を取得する。
		int result = 0;
		try {

			//ログの引数：削除するレコードをそれぞれ配列に格納する
			PreparedStatement st = getPreparedStatement(para);
			st.setInt(1, employeeID);
			ResultSet rs = st.executeQuery();
			while(rs.next()) {

				record_[0] = rs.getString("EmployeeID");
				record_[1] = rs.getString("FirstName");
				record_[2] = rs.getString("LastName");
				record_[3] = rs.getString("IPAddress");
				record_[4] = rs.getString("ViewIP");
				record_[5] = rs.getString("ViewID");
				record_[6] = rs.getString("LocalPhoneNumber");
				record_[7] = rs.getString("StatusID");
				record_[8] = rs.getString("LastUpdate");
				record_[9] = rs.getString("RoomID");
				record_[10] = rs.getString("Message");
				record_[11] = rs.getString("LeftX");
				record_[12] = rs.getString("LeftY");
				record_[13] = rs.getString("RightX");
				record_[14] = rs.getString("RightY");
				record_[15] = rs.getString("destination");
				record_[16] = rs.getString("startTime");
				record_[17] = rs.getString("endTime");
				record_[18] = rs.getString("neFLG");
				record_[19] = rs.getString("nrFLG");
			}




			// プリペアステートメントを取得し、実行SQLを渡す
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setInt(1, employeeID);

			result = statement.executeUpdate();

			// コミットを行う
			super.commit();
		} catch (Exception e) {
			super.rollback();
			throw e;
		}

		return result;
	}

	/**
	 * ●伝言削除処理を行う。指定されたMessageIDのタスクを削除する。
	 * @param messageID
	 * @return 削除件数
	 * @throws Exception
	 */
	public int deleteMessage(int messageID) throws Exception {

		//ログの引数：テーブル名
		tableName_ = "Message";

		//ログの引数：カラム
		clumName_ = new String[]{"MessageID","SendIP","ReceiveID","Message","Time","BookMark","ReadFLG"};

		//ログの引数：削除するレコードを格納する配列
		record_ = new String[clumName_.length];

		//ログの引数：削除するレコードを取り出すSQQL文
		String para = "SELECT * FROM seating.message WHERE MessageID = ?";



		String sql = "DELETE FROM seating.message where MessageID = ?";

		// SQLを実行してその結果を取得する。
		int result = 0;
		try {

			//ログの引数：削除するレコードをそれぞれ配列に格納する
			PreparedStatement st = getPreparedStatement(para);
			st.setInt(1, messageID);
			ResultSet rs = st.executeQuery();
			while(rs.next()) {

				record_[0] = rs.getString("MessageID");
				record_[1] = rs.getString("SendIP");
				record_[2] = rs.getString("ReceiveID");
				record_[3] = rs.getString("Message");
				record_[4] = rs.getString("Time");
				record_[5] = rs.getString("BookMark");
				record_[6] = rs.getString("ReadFLG");
			}

			// プリペアステートメントを取得し、実行SQLを渡す
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setInt(1, messageID);

			result = statement.executeUpdate();

			// コミットを行う
			super.commit();
		} catch (Exception e) {
			super.rollback();
			throw e;
		}
		return result;
	}

	/**
	 * ●部署削除処理を行う。 
	 * @param departmentID
	 * @return 削除件数
	 * @throws Exception
	 */
	public int deleteDepartment(int departmentID) throws Exception {

		//ログの引数：テーブル名
		tableName_ = "Message";

		//ログの引数：カラム
		clumName_ = new String[]{"DepartmentID","DepartmentName"};

		//ログの引数：削除するレコードを格納する配列
		record_ = new String[clumName_.length];

		//ログの引数：削除するレコードを取り出すSQQL文
		String para = "SELECT * FROM seating.Department WHERE DepartmentID = ?";


		String sql = "DELETE FROM seating.Department where DepartmentID = ?";

		// SQLを実行してその結果を取得する。
		int result = 0;
		try {

			//ログの引数：削除するレコードをそれぞれ配列に格納する
			PreparedStatement st = getPreparedStatement(para);
			st.setInt(1, departmentID);
			ResultSet rs = st.executeQuery();
			while(rs.next()) {

				record_[0] = rs.getString("DepartmentID");
				record_[1] = rs.getString("DepartmentName");
			}

			// プリペアステートメントを取得し、実行SQLを渡す
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setInt(1, departmentID);

			result = statement.executeUpdate();
			// コミットを行う
			super.commit();
		} catch (Exception e) {
			super.rollback();
			throw e;
		}
		return result;
	}
	
	/**
	 * ●部屋の削除。
	 * @param roomID
	 * @return 削除件数
	 * @throws Exception
	 */
	public int deleteRoom(int roomID) throws Exception {

		//ログの引数：テーブル名
		tableName_ = "Room";

		//ログの引数：カラム
		clumName_ = new String[]{"RoomID,RoomName,Width,Height"};

		//ログの引数：削除するレコードを格納する配列
		record_ = new String[clumName_.length];

		//削除するレコードを取り出す取り出すSQL文
		String para = "SELECT * FROM seating.employee WHERE RoomID = ?";

		String sql = "DELETE FROM seating.room where RoomID = ?";

		// SQLを実行してその結果を取得する。
		int result = 0;
		try {
			// プリペアステートメントを取得し、実行SQLを渡す
			PreparedStatement st = getPreparedStatement(para);
			st.setInt(1, roomID);
			ResultSet rs = st.executeQuery();
			while(rs.next()) {

				record_[0] = rs.getString("RoomID");
				record_[1] = rs.getString("RoomName");
				record_[2] = rs.getString("Width");
				record_[3] = rs.getString("Height");
			}
			// プリペアステートメントを取得し、実行SQLを渡す
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setInt(1, roomID);

			result = statement.executeUpdate();

			// コミットを行う
			super.commit();
		} catch (Exception e) {
			super.rollback();
			throw e;
		}
	return result;
	}

	/**
	 * 社内共有電話の削除
	 * @param phoneID
	 * @return 削除件数
	 * @throws Exception
	 */
	public int deletePhone(int PhoneID) throws Exception {

		//ログの引数：テーブル名
		tableName_ = "RentalPhone";

		//ログの引数：カラム
		clumName_ = new String[]{"PhoneID,TelNo,EmployeeID,Memo"};

		//ログの引数：削除するレコードを格納する配列
		record_ = new String[clumName_.length];

		//削除するレコードを取り出す取り出すSQL文
		String para = "SELECT * FROM seating.employee WHERE PhoneID = ?";

		String sql = "DELETE FROM seating.message where PhoneID = ?";

		// SQLを実行してその結果を取得する。
		int result = 0;
		try {
			// プリペアステートメントを取得し、実行SQLを渡す
			PreparedStatement st = getPreparedStatement(para);
			st.setInt(1, PhoneID);
			ResultSet rs = st.executeQuery();
			while(rs.next()) {

				record_[0] = rs.getString("PhoneID");
				record_[1] = rs.getString("TelNo");
				record_[2] = rs.getString("EmployeeID");
				record_[3] = rs.getString("Memo");
			}
			// プリペアステートメントを取得し、実行SQLを渡す
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setInt(1, PhoneID);

			result = statement.executeUpdate();
			// コミットを行う
			super.commit();
		} catch (Exception e) {
			super.rollback();
			throw e;
		}
	return result;
	}

	/**
	 * ステータス凡例の削除
	 * @param StatusID
	 * @return 削除件数
	 * @throws Exception
	 */
	public int deleteStatus(int StatusID) throws Exception {
		//ログの引数：テーブル名
		tableName_ = "Status";

		//ログの引数：カラム
		clumName_ = new String[]{"StatusID,Status,Color"};

		//ログの引数：削除するレコードを格納する配列
		record_ = new String[clumName_.length];

		//削除するレコードを取り出す取り出すSQL文
		String para = "SELECT * FROM seating.employee WHERE StatusID = ?";

		String sql = "DELETE FROM seating.status where StatusID = ?";

		// SQLを実行してその結果を取得する。
		int result = 0;
		try {
			// プリペアステートメントを取得し、実行SQLを渡す
			PreparedStatement st = getPreparedStatement(para);
			st.setInt(1, StatusID);
			ResultSet rs = st.executeQuery();
			while(rs.next()) {

				record_[0] = rs.getString("StatusID");
				record_[1] = rs.getString("Status");
				record_[2] = rs.getString("Color");
			}

			// プリペアステートメントを取得し、実行SQLを渡す
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setInt(1, StatusID);

			result = statement.executeUpdate();

			// コミットを行う
			super.commit();
		} catch (Exception e) {
			super.rollback();
			throw e;
		}
	return result;
	}

	/**
	 * 掲示板の内容を削除
	 * @param messageID
	 * @return 削除件数
	 * @throws Exception
	 */
	public int deleteBulletin(int BulletinID) throws Exception {

		//ログの引数：テーブル名
		tableName_ = "Bulletin";

		//ログの引数：カラム
		clumName_ = new String[]{"BulletinID,Bulletin,RoomID,Ipaddress,update"};

		//ログの引数：削除するレコードを格納する配列
		record_ = new String[clumName_.length];

		//削除するレコードを取り出す取り出すSQL文
		String para = "SELECT * FROM seating.employee WHERE PhoneID = ?";

		String sql = "DELETE FROM seating.message where BulletinID = ?";

		// SQLを実行してその結果を取得する。
		int result = 0;
		try {
			// プリペアステートメントを取得し、実行SQLを渡す
			PreparedStatement st = getPreparedStatement(para);
			st.setInt(1, BulletinID);
			ResultSet rs = st.executeQuery();
			while(rs.next()) {

				record_[0] = rs.getString("BulletinID");
				record_[1] = rs.getString("Bulletin");
				record_[2] = rs.getString("RoomID");
				record_[3] = rs.getString("Ipaddress");
				record_[4] = rs.getString("update");
			}

			// プリペアステートメントを取得し、実行SQLを渡す
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setInt(1, BulletinID);

			result = statement.executeUpdate();

			// コミットを行う
			super.commit();
		} catch (Exception e) {
			super.rollback();
			throw e;
		}
	return result;
	}

	/**
	 * リンクを削除
	 * @param LinkID
	 * @return 削除件数
	 * @throws Exception
	 */
	public int deleteLink(int linkID) throws Exception {

		String sel = "SELECT RoomID, Num FROM seating.link WHERE LinkID = ?";
		
		String sql = "DELETE FROM seating.link where LinkID = ?";

		String upd = "UPDATE seating.link SET Num = Num - 1 WHERE RoomID = ? AND Num > ?";

		// SQLを実行してその結果を取得する。
		int result = 0;
		int r_id = 0;
		int num = 0;
		try {
			
			PreparedStatement select = getPreparedStatement(sel);
			select.setInt(1, linkID);
			ResultSet rs = select.executeQuery();
			rs.next();
			r_id = rs.getInt("RoomID");
			num = rs.getInt("Num");
			
			// プリペアステートメントを取得し、実行SQLを渡す
			PreparedStatement statement = getPreparedStatement(sql);
			statement.setInt(1, linkID);
			
			PreparedStatement update = getPreparedStatement(upd);
			update.setInt(1, r_id);
			update.setInt(2, num);
			update.executeUpdate();

			result = statement.executeUpdate();
			// コミットを行う
			super.commit();
		} catch (Exception e) {
			super.rollback();
			throw e;
		}
	return result;
	}
	
	
	/** -------取得メソッド一覧-------　**/

	/**
	 * 表示する社員IDを指定して、個人詳細を返す。
	 * @param EmployeeID
	 * @return
	 * @throws Exception
	 */
	public TodoValueObject getDetail(int id) throws Exception {
		TodoValueObject vo = new TodoValueObject();
		//　マウスオーバーされた人の詳細情報を取得する
		String sql = "SELECT emp.EmployeeID, FirstName, LastName, FirstKana, LastKana, RoomID, IPAddress, ViewIP, ViewID, LocalPhoneNumber, Status," +
				"Message, det.Contact, det.Detail FROM seating.Employee emp JOIN seating.Status sta ON emp.StatusID = sta.StatusID LEFT OUTER JOIN seating.employeedetail det " +
				"ON emp.EmployeeID = det.EmployeeID WHERE emp.EmployeeID = ?";

		// プリペアステートメントを取得し、実行SQLを渡す
		PreparedStatement statement = getPreparedStatement(sql);

		statement.setInt(1, id);
		// SQLを実行してその結果を取得する。
		ResultSet rs = statement.executeQuery();

		// 検索結果の行数分フェッチを行い、取得結果をValueObjectへ格納する
		while (rs.next()) {
			vo.setEmployeeID(rs.getInt("EmployeeID"));
			vo.setFirstName(rs.getString("FirstName"));
			vo.setLastName(rs.getString("LastName"));
			vo.setFirstKana(rs.getString("FirstKana"));
			vo.setLastKana(rs.getString("LastKana"));
			vo.setRoomID(rs.getInt("RoomID"));
			vo.setIpAddress(rs.getString("IPAddress"));
			vo.setViewIP(rs.getBoolean("ViewIP"));
			vo.setViewID(rs.getBoolean("ViewID"));
			vo.setLocalPhoneNumber(rs.getString("LocalPhoneNumber"));
			vo.setStatus(rs.getString("Status"));
			vo.setMessage(rs.getString("Message"));
			vo.setContact(rs.getString("Contact"));
			vo.setDetail(rs.getString("Detail"));
		}
		return vo;
	}

	/**
	 * 表示する社員IDを指定して、連絡先詳細を返す。
	 * @param EmployeeID
	 * @return
	 * @throws Exception
	 */
	public List<TodoValueObject> getEmpDetail(int id) throws Exception {
		List<TodoValueObject> returnList = new ArrayList<TodoValueObject>();
		//　マウスオーバーされた人の詳細情報を取得する
		String sql = "SELECT Contact, Detail FROM seating.EmployeeDetail WHERE EmployeeID = ?";

		// プリペアステートメントを取得し、実行SQLを渡す
		PreparedStatement statement = getPreparedStatement(sql);

		statement.setInt(1, id);
		// SQLを実行してその結果を取得する。
		ResultSet rs = statement.executeQuery();

		// 検索結果の行数分フェッチを行い、取得結果をValueObjectへ格納する
		while (rs.next()) {
			TodoValueObject vo = new TodoValueObject();
			vo.setContact(rs.getString("Contact"));
			vo.setDetail(rs.getString("Detail"));
			returnList.add(vo);
		}
		return returnList;
	}
	
	/**
	 * 掲示板の内容を返す。
	 * @return
	 * @throws Exception
	 */
	public List<TodoValueObject> getBulletin() throws Exception {
		List<TodoValueObject> returnList = new ArrayList<TodoValueObject>();

		String sql = "SELECT bull.Bulletin, bull.Update, emp.FirstName FROM seating.bulletin bull JOIN " +
				"seating.employee emp ON bull.IPAddress = emp.IPAddress ORDER BY bull.Update";

		// プリペアステートメントを取得し、実行SQLを渡す
		PreparedStatement statement = getPreparedStatement(sql);
		// SQLを実行してその結果を取得する。
		ResultSet rs = statement.executeQuery();

		// 検索結果の行数分フェッチを行い、取得結果をValueObjectへ格納する
		while (rs.next()) {
			TodoValueObject vo = new TodoValueObject();
			vo.setBulletin(rs.getString("Bulletin"));
			vo.setUpdate(rs.getDate("Update"));
			vo.setFirstName(rs.getString("FirstName"));
			returnList.add(vo);
		}
		return returnList;
	}

	/**
	 * ●表示する部屋IDを指定して、部屋詳細を返す。
	 * @param roomID
	 * @return
	 * @throws Exception
	 */
	public TodoValueObject getRoom(int roomID) throws Exception {
		TodoValueObject vo = new TodoValueObject();

		String sql = "SELECT room.RoomID,room.RoomName,room.Width,room.Height,dep.DepartmentName FROM seating.room room JOIN " +
				"seating.roomanddepartment rad ON room.RoomID = rad.RoomID JOIN seating.department dep " +
				"ON rad.DepartmentID = dep.DepartmentID WHERE room.RoomID = ?";


		// プリペアステートメントを取得し、実行SQLを渡す
		PreparedStatement statement = getPreparedStatement(sql);

		statement.setInt(1, roomID);
		// SQLを実行してその結果を取得する。
		ResultSet rs = statement.executeQuery();

		// 検索結果の行数分フェッチを行い、取得結果をValueObjectへ格納する
		while (rs.next()) {
			vo.setRoomID(rs.getInt("RoomID"));
			vo.setRoomName(rs.getString("RoomName"));
			vo.setWidth(rs.getInt("Width"));
			vo.setHeight(rs.getInt("Height"));
			vo.setDepartmentName(rs.getString("DepartmentName"));
		}
		return vo;
	}
	
	/**
	 * 部屋を返す。
	 * @return
	 * @throws Exception
	 */
	public List<TodoValueObject> getRoom() throws Exception {
		List<TodoValueObject> returnList = new ArrayList<TodoValueObject>();

		String sql = "SELECT RoomID, RoomName FROM seating.room";


		// プリペアステートメントを取得し、実行SQLを渡す
		PreparedStatement statement = getPreparedStatement(sql);

		// SQLを実行してその結果を取得する。
		ResultSet rs = statement.executeQuery();

		// 検索結果の行数分フェッチを行い、取得結果をValueObjectへ格納する
		while (rs.next()) {
			TodoValueObject vo = new TodoValueObject();
			vo.setRoomID(rs.getInt("RoomID"));
			vo.setRoomName(rs.getString("RoomName"));
			returnList.add(vo);
		}
		return returnList;
	}

	/**
	 * ●表示する部屋IDを指定して、部屋詳細を返す。
	 * @param roomID
	 * @return
	 * @throws Exception
	 */
	public List<TodoValueObject> getRoomAll() throws Exception {
		List<TodoValueObject> returnList = new ArrayList<TodoValueObject>();

		String sql = "SELECT r.RoomID,r.RoomName,r.Width,r.Height,d.DepartmentName FROM seating.room r " +
				"LEFT OUTER JOIN seating.department d ON r.DepartmentID = d.DepartmentID";


		// プリペアステートメントを取得し、実行SQLを渡す
		PreparedStatement statement = getPreparedStatement(sql);

		// SQLを実行してその結果を取得する。
		ResultSet rs = statement.executeQuery();

		// 検索結果の行数分フェッチを行い、取得結果をValueObjectへ格納する
		while (rs.next()) {
			TodoValueObject vo = new TodoValueObject();
			vo.setRoomID(rs.getInt("RoomID"));
			vo.setRoomName(rs.getString("RoomName"));
			vo.setWidth(rs.getInt("Width"));
			vo.setHeight(rs.getInt("Height"));
			vo.setDepartmentName(rs.getString("DepartmentName"));
			returnList.add(vo);
		}
		return returnList;
	}

	/**
	 * テーブル情報を持たない社員を探す
	 * @param roomID
	 * @return
	 * @throws Exception
	 */
	public TodoValueObject getEmployee() throws Exception {
		TodoValueObject vo = new TodoValueObject();

		String sql = "SELECT FirstName,LastName FROM seating.Employee WHERE RoomID IS NOT NULL";

		// プリペアステートメントを取得し、実行SQLを渡す
		PreparedStatement statement = getPreparedStatement(sql);

		// SQLを実行してその結果を取得する。
		ResultSet rs = statement.executeQuery();

		// 検索結果の行数分フェッチを行い、取得結果をValueObjectへ格納する
		while (rs.next()) {
			vo.setFirstName(rs.getString("FirstName"));
			vo.setLastName(rs.getString("LastName"));
		}
		return vo;
	}

	/**
	 * 伝言版既読or未読
	 * @param roomID
	 * @return
	 * @throws Exception
	 */
	public ArrayList<Integer> getReadFLG(int roomID) throws Exception {
		ArrayList<Integer> returnList = new ArrayList<Integer>();

		String sql = "SELECT ReceiveID FROM seating.Message msg JOIN seating.Employee emp ON " +
				"msg.ReceiveID = emp.EmployeeID JOIN seating.Room ON emp.RoomID = room.RoomID " +
				"WHERE msg.ReadFLG IS FALSE AND room.RoomID = ? GROUP BY msg.ReceiveID";

		// プリペアステートメントを取得し、実行SQLを渡す
		PreparedStatement statement = getPreparedStatement(sql);
		statement.setInt(1, roomID);
		// SQLを実行してその結果を取得する。
		ResultSet rs = statement.executeQuery();

		// 検索結果の行数分フェッチを行い、取得結果を格納する
		while (rs.next()) {
			returnList.add(rs.getInt("ReceiveID"));
		}
		return returnList;
	}


	/**
	 * 連絡先詳細を返す。
	 * @return
	 * @throws Exception
	 */
	public List<TodoValueObject> getRentalPhone() throws Exception {
		List<TodoValueObject> returnList = new ArrayList<TodoValueObject>();

		//TO_DATE(LastUpdate,'yyyy/mm/dd hh24:mi:ss')は何故か頭にsampleがついて使えない
		//DATE_FORMAT(LastUpdate, '%W %M %Y')とFROM_UNIXTIME(LastUpdate)はLastUpdateが見つからないという
		String sql = "SELECT TelNo,EmployeeID,Memo,PhoneID,PhoneStatus,LastUpdate FROM seating.RentalPhone";

		// プリペアステートメントを取得し、実行SQLを渡す
		PreparedStatement statement = getPreparedStatement(sql);

		// SQLを実行してその結果を取得する。
		ResultSet rs = statement.executeQuery();

		// 検索結果の行数分フェッチを行い、取得結果をValueObjectへ格納する
		while (rs.next()) {
			TodoValueObject vo = new TodoValueObject();
			vo.setTelNo(rs.getString("TelNo"));
			vo.setEmployeeID(rs.getInt("EmployeeID"));
			vo.setMemo(rs.getString("Memo"));
			vo.setPhoneID(rs.getInt("PhoneID"));
			vo.setPhoneStatus(rs.getString("PhoneStatus"));
			vo.setLastUpdate(rs.getDate("LastUpdate"));
			returnList.add(vo);
		}
		return returnList;
	}
	
	/**
	 * 部署一覧を返す。
	 * @return
	 * @throws Exception
	 */
	public List<TodoValueObject> getDepartment() throws Exception {
		List<TodoValueObject> returnList = new ArrayList<TodoValueObject>();

		String sql = "SELECT DepartmentID, DepartmentName FROM seating.department";

		// プリペアステートメントを取得し、実行SQLを渡す
		PreparedStatement statement = getPreparedStatement(sql);

		// SQLを実行してその結果を取得する。
		ResultSet rs = statement.executeQuery();

		// 検索結果の行数分フェッチを行い、取得結果をValueObjectへ格納する
		while (rs.next()) {
			TodoValueObject vo = new TodoValueObject();
			vo.setDepartmentID(rs.getInt("DepartmentID"));
			vo.setDepartmentName(rs.getString("DepartmentName"));
			returnList.add(vo);
		}
		return returnList;
	}

	/**
 	 * ステータスのリストを返す。
	 * @param roomID
	 * @return
	 * @throws Exception
	 */
	public List<TodoValueObject> getStatus() throws Exception {
		List<TodoValueObject> returnList = new ArrayList<TodoValueObject>();

		String sql = "SELECT StatusID, Status, Color FROM seating.Status";

		// プリペアステートメントを取得し、実行SQLを渡す
		PreparedStatement statement = getPreparedStatement(sql);

		// SQLを実行してその結果を取得する。
		ResultSet rs = statement.executeQuery();

		// 検索結果の行数分フェッチを行い、取得結果をValueObjectへ格納する
		while (rs.next()) {
			TodoValueObject vo = new TodoValueObject();
			vo.setStatusID(rs.getInt("StatusID"));
			vo.setStatus(rs.getString("Status"));
			vo.setColor(rs.getString("Color"));
			returnList.add(vo);
		}
		return returnList;
	}
		
	/**
	 * ●伝言詳細の取得
	 * @return
	 * @throws Exception
	 */
	public List<TodoValueObject> getMessage() throws Exception {
		List<TodoValueObject> returnlist = new ArrayList<TodoValueObject>();

		String sql = "SELECT m.MessageID,m.SendIP,m.ReceiveID,m.Message,m.Time,m.BookMark,m.ReadFLG,m.Subject,e.FirstName FROM seating.Message m " +
				"JOIN seating.Employee e ON m.SendIP = e.IPAddress";

		// プリペアステートメントを取得し、実行SQLを渡す
		PreparedStatement statement = getPreparedStatement(sql);
		
		// SQLを実行してその結果を取得する。
		ResultSet rs = statement.executeQuery();

		// 検索結果の行数分フェッチを行い、取得結果をValueObjectへ格納する
		while (rs.next()) {
			TodoValueObject vo = new TodoValueObject();
			vo.setMessageID(rs.getInt("MessageID"));
			vo.setSendIP(rs.getString("SendIP"));
			vo.setReceiveID(rs.getInt("ReceiveID"));
			vo.setMessage(rs.getString("Message"));
			vo.setTime(rs.getTime("Time"));
			vo.setBookMark(rs.getBoolean("BookMark"));
			vo.setReadFLG(rs.getBoolean("ReadFLG"));
			vo.setSubject(rs.getString("Subject"));
			vo.setFirstName(rs.getString("FirstName"));
			returnlist.add(vo);
		}
		return returnlist;
	}
	
	/**
	 * ●リンクの取得
	 * @return
	 * @throws Exception
	 */
	public List<TodoValueObject> getLink(int roomID) throws Exception {
		List<TodoValueObject> returnlist = new ArrayList<TodoValueObject>();
		
		String sql = "SELECT LinkID, Num, PageName, LinkURL FROM seating.link WHERE RoomID = ? ORDER BY Num DESC";
		
		// プリペアステートメントを取得し、実行SQLを渡す
		PreparedStatement statement = getPreparedStatement(sql);
		statement.setInt(1, roomID);
		ResultSet rs = statement.executeQuery();
		
		// 検索結果の行数分フェッチを行い、取得結果をValueObjectへ格納する
		while (rs.next()) {
			TodoValueObject vo = new TodoValueObject();
			vo.setLinkID(rs.getInt("LinkID"));
			vo.setNum(rs.getInt("Num"));
			vo.setPageName(rs.getString("PageName"));
			vo.setLinkURL(rs.getString("LinkURL"));
			returnlist.add(vo);
		}
		return returnlist;
	}
	
	/**
	 * 返信メールアドレスと名前の取得
	 * @param sendIP
	 * @return
	 * @throws Exception
	 */
	public TodoValueObject getReply(int sendIP, int receiveID) throws Exception {
		TodoValueObject vo = new TodoValueObject();
		String sql = "SELECT (SELECT FirstName FROM seating.employee WHERE EmployeeID = ?) AS Name, Contact " +
				"FROM seating.employeedetail WHERE EmployeeID = ?";
		
		// プリペアステートメントを取得し、実行SQLを渡す
		PreparedStatement statement = getPreparedStatement(sql);
		statement.setInt(1, receiveID);
		statement.setInt(2, sendIP);
		// SQLを実行してその結果を取得する。
		ResultSet rs = statement.executeQuery();

		// 検索結果の行数分フェッチを行い、取得結果を格納する
		while (rs.next()) {
			vo.setContact(rs.getString("Contact"));
			vo.setFirstName(rs.getString("Name"));
		}
		return vo;
	}
	
	/**
	 * @return tableName_
	 */
	public String getTableName_() {
		return tableName_;
	}


	/**
	 * @param tableName_ セットする tableName_
	 */
	public void setTableName_(String tableName) {
		this.tableName_ = tableName;
	}


	/**
	 * @return clumName_
	 */
	public String[] getClumName_() {
		return clumName_;
	}


	/**
	 * @param clumName_ セットする clumName_
	 */
	public void setClumName_(String[] clumName) {
		this.clumName_ = clumName;
	}


	/**
	 * @return record_
	 */
	public String[] getRecord_() {
		return record_;
	}


	/**
	 * @param record_ セットする record_
	 */
	public void setRecord_(String[] record) {
		this.record_ = record;
	}


	/**
	 * @return before_
	 */
	public String[] getBefore_() {
		return before_;
	}


	/**
	 * @param before_ セットする before_
	 */
	public void setBefore_(String[] before) {
		this.before_ = before;
	}


	/**
	 * @return after_
	 */
	public String[] getAfter_() {
		return after_;
	}


	/**
	 * @param after_ セットする after_
	 */
	public void setAfter_(String[] after) {
		this.after_ = after;
	}


	/**
	 * @return key_
	 */
	public String[] getKey_() {
		return key_;
	}




}
